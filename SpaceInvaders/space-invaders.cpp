﻿# include <GL/glew.h>
# include <GL/freeglut.h>
#include <cmath>
#include <sstream>
using namespace std;

#define PI 3.14159265358979323846
#define nvertices 24
int width = 1280; // dimensioni iniziali della finestra 
int height = 720;


typedef struct { double r, g, b, a; } ColorRGBA; // in formato (255,255,255,255)

												 //Colori Ufo

ColorRGBA color_metaldark = { 110,110,110,255 };
ColorRGBA color_metallight = { 184,184,184,255 };

ColorRGBA color_cabindark = { 66,134,244,255 };
ColorRGBA color_cabinlight = { 158,195,255,255 };

//posizione luna
int xluna = 320;
int yluna = 640;

int shakeMoon=0, HitMoon=0;


//parametri ufo
float FloatingAngle = 0;
double VelocitaOrizzontale = 0; //velocita orizzontale (pixel per frame)
double accelerazione = 1; // forza di accelerazione data dalla tastiera
double decelerazione = 1; //decelerazione in assenza di input
float posx = width / 2; //coordinate sul piano della posizione iniziale della navicella
float  posy = height*0.1;
float balance_position = posy;
float floating_range = 15;
int frame_animazione = 0; //indica il frame dell'animazione della fluttuazione
float angle_offset = 0;  //indica la direzione in cui punta la navicella quando si sposta


float posx_Proiettile, posy_Proiettile;
float posx_Proiettile_Mondo, posy_Proiettile_Mondo;

//Gestione nemici
int nRighe=3;
float nemici_per_riga=5;
float passo_Nemici;
float posxN, posyN;
float dxN=0, dyN=0;
int frameN = 0;

//Funzioni di utilit�

// da gradi in radianti

double  degtorad(double angle) {
	return angle * PI / 180;
}

//Da radianti in gradi
double  radtodeg(double angle) {
	return angle / (PI / 180);
}

// ritorna un numero casuale pescato in un certo range
double  random_range(double min, double max) {
	return min + static_cast <double> (rand()) / (static_cast <double> (RAND_MAX / (max - min)));
}

string floatToString(float value) {
	stringstream ss;
	ss << value;
	return ss.str();
}

void bitmap_output(int x, int y, int z, string s,
	void *font)
{
	int i;
	glRasterPos3f(x, y, 0);

	for (i = 0; i < s.length(); i++)
		glutBitmapCharacter(font, s[i]);
}


//-----------------------------------------------------------------------------------------

void disegna_piano(float x, float y, float width, float height, ColorRGBA color_top, ColorRGBA color_bot) {

	glBegin(GL_POLYGON);

	glColor4d(color_top.r / 255, color_top.g / 255, color_top.b / 255, color_top.a / 255);
	glVertex2f(x, y);
	glVertex2f(x + width, y);

	glColor4d(color_bot.r / 255, color_bot.g / 255, color_bot.b / 255, color_bot.a / 255);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);

	glEnd();
}

//Disegna un cerchio in coordinate dell'oggetto

void disegna_cerchio(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / nvertices;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= nvertices; i++)
		glVertex2f(cos((double)i *stepA), sin((double)i *stepA));
	glEnd();
}

//Disegna un ellisse in coordinate dell'oggetto

void disegna_ellisse(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / nvertices;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= nvertices; i++)
		glVertex2f(cos((double)i *stepA), 2*sin((double)i *stepA));
	glEnd();
}


// TASTI: A D

bool pressing_left = false;
bool pressing_right = false;

void updateProiettile(int value) {

	//Ascissa proiettile durante lo sparo
	posx_Proiettile = 0;
	//Ordinata proiettile durante lo sparo
	posy_Proiettile++;

	if (posy_Proiettile <= 50)
		glutTimerFunc(5, updateProiettile, 0);
	else
		posy_Proiettile = 0;
}


void keyboardPressedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case ' ':
		updateProiettile(0);
		break;
	case 'A':
	case 'a':
		pressing_left = true;
		break;
	case 'D':
	case 'd':
		pressing_right = true;
		break;

	case 27:
		exit(0);
		break;

	default:
		break;
	}
}

void keyboardReleasedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'A':
	case 'a':
		pressing_left = false;
		break;
	case 'D':
	case 'd':
		pressing_right = false;
		break;



	default:
		break;
	}
}


double lerp(double a, double b, double amount) {

	//Interpolazione lineare tra a e b secondo amount

	return (1 - amount)*a + amount * b;
}


void updateNemici(int a) {
	frameN++;
	if (frameN % 30 == 0) {
		dxN--;
		dyN--;
	}
	glutPostRedisplay();
	glutTimerFunc(5, updateNemici, 0);
}



void update(int a)
{
	bool moving = false;
	//Movimento della navicella in orizzontale

	frame_animazione++;
	if (frame_animazione >= 360) {
		frame_animazione -= 360;
	}

	if (pressing_left)
	{
		VelocitaOrizzontale -= accelerazione;
		moving = true;
	}

	if (pressing_right)
	{
		VelocitaOrizzontale += accelerazione;
		moving = true;
	}



	if (!moving) {   //Se non mi sto muovendo con i tasti a e d decremento od incremento la velocita' iniziale fino a portarla
					 // a zero e l'ufo continua a roteare sul posto

		if (VelocitaOrizzontale > 0)
		{
			VelocitaOrizzontale -= 1;
			if (VelocitaOrizzontale < 0)
				VelocitaOrizzontale = 0;
		}

		if (VelocitaOrizzontale < 0)
		{
			VelocitaOrizzontale += 1;
			if (VelocitaOrizzontale > 0)
				VelocitaOrizzontale = 0;
		}
	}



	//Aggioramento della posizione in x dell'ufo, che regola il movimento orizzontale

	posx += VelocitaOrizzontale;



	//Se l'ufo  assume una posizione in x minore di 0 o maggiore di width dello schermo
	//facciamo ritornare indietro l'ufo

	if (posx < 0) {
		posx = 0;
		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
	}

	if (posx >width) {
		posx = width;
		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
	}

	posy = balance_position + sin(degtorad(frame_animazione))*floating_range;
	FloatingAngle = cos(degtorad(frame_animazione))*angle_offset - VelocitaOrizzontale*1.3;

	glutPostRedisplay();
	glutTimerFunc(15, update, 0);
}


void disegnaNemici()
{
	//Disegna corpo
	disegna_cerchio(color_metaldark, color_metallight);
	glPushMatrix();
	glTranslatef(0.05, 0.1, 0);
	glScalef(0.7, 0.7, 1);
	disegna_cerchio({ 255,255,255,32 }, { 255,255,255,32 });
	glPopMatrix();


	// Disegna Decorazione
	glPushMatrix();
	glTranslatef(0, 1, 0);
	glBegin(GL_TRIANGLES);
	glColor4f(1.0, 0.0, 1.0, 0.4);
	glVertex2f(0.0, -1);
	glColor4f(1.0, 0.0, 0.4, 1);
	glVertex2f(1, 0);
	glColor4f(1.0, 0.2, 0.1, 1);
	glVertex2f(-1, 0);
	glEnd();
	glPopMatrix();
	//Disegna Cabina
	glPushMatrix();
	glTranslatef(0, 0.2, 0);
	glScalef(0.5, 0.6, 1);
	disegna_cerchio(color_cabinlight, color_cabindark);
	glPopMatrix();


}


void disegnaUfo()
{
	//Disegna corpo
	disegna_cerchio(color_metaldark, color_metallight);
	glPushMatrix();
	glTranslatef(0.05, 0.1, 0);
	glScalef(0.7, 0.7, 1);
	disegna_cerchio({ 255,255,255,32 }, { 255,255,255,32 });
	glPopMatrix();


	// Disegna Decorazione
	glPushMatrix();
	glTranslatef(0, 1, 0);
	glBegin(GL_TRIANGLES);
	glColor4f(0.0, 0.0, 1.0, 0.4);
	glVertex2f(0.0, -1);
	glColor4f(0.0, 0.4, 1, 1);
	glVertex2f(1, 0);
	glColor4f(0.0, 0.2, 1, 1);
	glVertex2f(-1, 0.0);
	glEnd();
	glPopMatrix();
	//Disegna Cabina
	glPushMatrix();
	glTranslatef(0, 0.2, 0);
	glScalef(0.5, 0.6, 1);
	disegna_cerchio(color_cabinlight, color_cabindark);
	glPopMatrix();

	//Disegna Proiettile
	glPointSize(10.0);
	glPushMatrix();
	glTranslatef(posx_Proiettile, -posy_Proiettile, 0.0);
	glBegin(GL_POINTS);
	glVertex3f(0.0, 0, 0);
	glEnd();

	//Matrice di ModelView che agisce sul proiettile
	float matrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

	float c_p_O[4];	//coord omogenee del proiettile
	c_p_O[0] = 0;
	c_p_O[1] = 0;
	c_p_O[2] = 0;
	c_p_O[3] = 1;

	//Calcolare le coordinate assunte dal proiettile nel mondo
	float c_p_M[4];
	for (int i = 0; i < 4; i++) {
		c_p_M[i] = 0;
		for (int j = 0; j < 4; j++) {
			c_p_M[i] += matrix[j * 4 + i] + c_p_O[j];
		}
	}

	posx_Proiettile_Mondo = c_p_M[0];
	posy_Proiettile_Mondo = c_p_M[1];

	glPopMatrix();
}

void disegnaSpaceShip()
{
	//Disegna corpo
	disegna_cerchio(color_metaldark, color_metallight);
	glPushMatrix();
	glTranslatef(0.05, 0.1, 0);
	glScalef(0.7, 0.7, 1);
	disegna_cerchio({ 255,255,255,32 }, { 255,255,255,32 });
	glPopMatrix();


	// Disegna Decorazione
	glPushMatrix();
	glTranslatef(0, 1, 0);
	glBegin(GL_TRIANGLES);
	glColor4f(0.0, 0.0, 1.0, 0.4);
	glVertex2f(0.0, -1);
	glColor4f(0.0, 0.4, 1, 1);
	glVertex2f(1, 0);
	glColor4f(0.0, 0.2, 1, 1);
	glVertex2f(-1, 0.0);
	glEnd();
	glPopMatrix();
	//Disegna Cabina
	glPushMatrix();
	glTranslatef(0, 0.2, 0);
	glScalef(0.5, 0.6, 1);
	disegna_cerchio(color_cabinlight, color_cabindark);
	glPopMatrix();

	//Disegna Proiettile
	glPointSize(10.0);
	glPushMatrix();
	glTranslatef(posx_Proiettile, -posy_Proiettile, 0.0);
	glBegin(GL_POINTS);
	glVertex3f(0.0, 0, 0);
	glEnd();

	glPopMatrix();
}

// Drawing routine.
void drawScene(void)
{
	ColorRGBA c_top;
	ColorRGBA c_bot;
	//Definisce il colore con cui verr�  ripulito lo schermo
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//Pulisce lo schermo con il colore definito con la funzione glClearColor
	glClear(GL_COLOR_BUFFER_BIT);

	//Disegna luna
	c_top = { 255.0, 255.0, 255.0, 255.0 };
	c_bot = { 255.0, 220.0, 255.0, 255.0 };
	glPushMatrix();
	glTranslatef(random_range(-shakeMoon, shakeMoon), random_range(-shakeMoon, shakeMoon), 0);
	glTranslatef(xluna, yluna, 0);
	glScalef(30, 30, 1);
	disegna_cerchio(c_top, c_bot);
	glPopMatrix();
	//Disegna alone luna
	c_top = { 255.0, 255.0, 255.0, 0.0 };
	c_bot = { 255.0, 220.0, 255.0, 255.0 };
	glPushMatrix();
	glTranslatef(random_range(-shakeMoon, shakeMoon), random_range(-shakeMoon, shakeMoon), 0);
	glTranslated(xluna, yluna, 0);
	glScalef(80, 80, 1);
	disegna_cerchio(c_top, c_bot);
	glPopMatrix();
	if(shakeMoon>0)
		shakeMoon--;
	//brackground asteroids
	glPointSize(6.0);
	glColor4f(1.0, 0.0, 1.0, 1.0);
	glPushMatrix();
	glTranslatef(random_range(0, width), random_range(0, height), 1);
	glBegin(GL_POINTS);
	glVertex2f(0.0, 0.0);
	glEnd();
	glPopMatrix();
	//Disegna Ufo
	glPushMatrix();
	glTranslatef(posx, posy, 0);
	glScalef(55, 22.5, 1);
	glRotated(180, 0, 0, 1);
	glRotated(FloatingAngle, 0, 0, 1);
	disegnaUfo();
	glPopMatrix();
	//shake moon
	if (((posx_Proiettile_Mondo >= xluna - 30) && (posx_Proiettile_Mondo <= xluna + 30)) && ((posy_Proiettile_Mondo >= yluna - 30) && (posy_Proiettile_Mondo <= yluna + 30))) {
		shakeMoon = 30;
		HitMoon++;
		fprintf(stdout,"Moon hit: %d\r",HitMoon);
		posy_Proiettile = 100;
	}
	//Disegna nemici
	passo_Nemici = ((float)width) / nemici_per_riga;
	float pRighe = 50;
	for (int i = 0; i < nRighe; i++) {
		posyN = height - i*pRighe -50;
		for (int j = 0; j < nemici_per_riga; j++) {
			posxN=j*passo_Nemici+passo_Nemici/2;
			glPushMatrix();
			glTranslatef(posxN, posyN, 0);
			glTranslatef(dxN, dyN, 0);
			glScalef(55, 22.5, 1);
			glRotatef(FloatingAngle, 0, 0, 1);
			disegnaNemici();
			glPopMatrix();
		}
	}

	glutSwapBuffers();



}


// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void setup() {

}

int main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(3, 0);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Animazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);

	//Evento tastiera tasi premuti
	glutKeyboardFunc(keyboardPressedEvent);
	//Evento tastiera tasto rilasciato

	glutKeyboardUpFunc(keyboardReleasedEvent);

	//setup();

	//gestione animazione
	glutTimerFunc(11, update, 0);
	glutTimerFunc(11, updateNemici, 0);


	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
		return 1;
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();

	return 0;
}

