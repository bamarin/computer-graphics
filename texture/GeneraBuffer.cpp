
#include "stdafx.h"
#include "Strutture.h"
#include "Lib.h"

GLuint crea_VAO_Puntatori(GLuint VAO, Point* Vertici, int nv, ColorRGBA* Colori_vertici, Indici* Indici_vertici, int ni, CoordText* coord_uv)
{
	GLuint vertexbuffer, colorbuffer, indices_buffer, texture_buffer;

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	printf("Dentro VAO 1");

	//Cominciamo a Costruire i VBO di vertici, colori e indici dei vertici
	
	//Genero , rendo attivo, riempio ed assegno al layer 0 la geometria della mia primitva
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, nv * sizeof(Point), Vertici, GL_STATIC_DRAW);


	//Adesso carico il VBO nel layer 0
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 


	//Genero , rendo attivo, riempio ed assegno al layer 1 i colori di ogni vertice della primitiva
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, nv * sizeof(ColorRGBA), Colori_vertici, GL_STATIC_DRAW);

	//Adesso carico il VBO nel layer 1
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 

	glGenBuffers(1, &texture_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, texture_buffer);
	glBufferData(GL_ARRAY_BUFFER, nv * sizeof(CoordText), coord_uv, GL_STATIC_DRAW);

	//Adesso carico il VBO nel layer 1
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 

	//Genero un VBO per gli indici
	glGenBuffers(1, &indices_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ni * sizeof(Indici), Indici_vertici, GL_STATIC_DRAW);

	return VAO;
}

GLuint crea_VAO_Vector(GLuint VAO, vector <Point> vertici, vector <GLuint> indici, vector <ColorRGBA> Colori)
{
	GLuint vertexbuffer, colorbuffer, elements_buffer;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertici.size() * sizeof(Point), &vertici[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0, // attributo
		3,                 // numero di elementi per vertice,  (x,y,z)
		GL_FLOAT,          // il tipo di ogni elemento
		GL_FALSE,          // non normalizza il vaore
		0,                 // non ci sondati extra tra ogni posizione 
		0                  // offset del primo elemento
	);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, Colori.size() * sizeof(Colori), &Colori[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1, // attributo
		4,                 // numero di elementi per vertice, here (R,G,B,A)
		GL_FLOAT,          // tipo di ogni elemento
		GL_FALSE,          // normalizzazione FALSE
		0,                 //non ci sono dati extra tra ogni posizione
		0                  // offset del primo elemento
	);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//VBO di tipo indici
	glGenBuffers(1, &elements_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indici.size() * sizeof(GLuint), &indici[0], GL_STATIC_DRAW);


	return VAO;

}