// texture1.cpp : definisce il punto di ingresso dell'applicazione console.
//
#include "stdafx.h"
#include "Lib.h"
#include "ShaderMaker.h"
#include "geometria.h"
#include "GeneraBuffer.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"



Point  *verticesC, *verticesP, *verticesPiano;
GLuint idTex0, idTex1, idTex2;
ColorRGBA *vertex_colorsPiano;
Indici  *index_verticesPiano;
CoordText *coord_uv_piano;
mat4 View;
mat4 MV;
GLuint programId, programId1, programId2, IdentificativoProgramma;

mat4 Projection;
vec3 camera_pos(0, 0, 15), direzione(0.0, 0.0, -1.0), camera_up(0.0, 1.0, 0.0);

//Definizione dei VAO
GLuint  VAO_PIANO;

//Locazione della matrice ModelViewProjection nello shader
GLuint MatrixID;


GLuint texture2;

void costruisci_scena(void)
{

	verticesPiano = new Point[8];
	vertex_colorsPiano = new ColorRGBA[8];
	index_verticesPiano = new Indici[12];
	coord_uv_piano = new CoordText[8];
	crea_piano(verticesPiano, vertex_colorsPiano, index_verticesPiano, coord_uv_piano);

};

void genera_texture(void)
{
	unsigned int texture, texture1;


	glGenTextures(1, &texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load("topolino.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		printf("Tutto OK %d %d \n", width, height);
	}
	else
	{
		printf("Errore nel caricare la texture\n");
	}
	stbi_image_free(data);


	glGenTextures(1, &texture1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture1);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	stbi_set_flip_vertically_on_load(true);
	data = stbi_load("minnie.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		printf("Errore nel caricare la texture\n");
	}
	stbi_image_free(data);


	glGenTextures(1, &texture2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture2);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	stbi_set_flip_vertically_on_load(true);
	data = stbi_load("paesaggio.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		printf("Errore nel caricare la texture\n");
	}
	stbi_image_free(data);
}
// Initialization routine.
void INIT_VAO(void)
{
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	char* vertexShader = (char*)"vertexShader.glsl";
	char* fragmentShader = (char*)"fragmentShader.glsl";

	//Creo il programma in cui sono linkati i due shader compilati
	programId = ShaderMaker::createProgram(vertexShader, fragmentShader);

	//Lo utilizzo
	fragmentShader = (char*)"fragmentShader1.glsl";
	programId1 = ShaderMaker::createProgram(vertexShader, fragmentShader);


	fragmentShader = (char*)"fragmentShader2.glsl";
	programId2 = ShaderMaker::createProgram(vertexShader, fragmentShader);

	//Crea il VAO del Piano
	VAO_PIANO = crea_VAO_Puntatori(VAO_PIANO, verticesPiano, 4, vertex_colorsPiano, index_verticesPiano, 2, coord_uv_piano);

}
void keyInput(unsigned char key, int x, int y)
{
	
	if (key == '0')
	{
		IdentificativoProgramma = programId;
		glUseProgram(programId);
	}

	if (key == '1')
	{
		IdentificativoProgramma = programId1;
		glUseProgram(programId1);
	}
	if (key == '2')
	{
		IdentificativoProgramma = programId2;
		glUseProgram(programId2);
	}
	
	glutPostRedisplay();
}
	
	void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	//Genero la matrice della Telecamera, (matrice di Vista)
	mat4 MV, model;
	int loc = glGetUniformLocation(IdentificativoProgramma, "time");

	glUniform1f(loc, glutGet(GLUT_ELAPSED_TIME));

	View = lookAt(camera_pos, camera_pos + direzione, camera_up);
	//Richiedo a program la locazione della variabile MV

	MatrixID = glGetUniformLocation(IdentificativoProgramma, "MV");
	idTex0 = glGetUniformLocation(IdentificativoProgramma, "tex0");
	idTex1 = glGetUniformLocation(IdentificativoProgramma, "tex1");
	idTex2 = glGetUniformLocation(IdentificativoProgramma, "tex2");

	glUniform1i(idTex0, 0);
	glUniform1i(idTex1, 1);
	glUniform1i(idTex2, 2);

	glBindVertexArray(VAO_PIANO);
	model = mat4(1.0);
	model = translate(model, vec3(-2, 2, 0));
	model = rotate(model, radians(90.0f), vec3(1, 0, 0));
	model = scale(model, vec3(3.0, 3.0, 3.0));
	MV = Projection * View*model;
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, value_ptr(MV));
	glDrawElements(GL_TRIANGLES, sizeof(index_verticesPiano) * sizeof(Indici), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

		
	//Rilascio il VAO
	glBindVertexArray(0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);


	glutSwapBuffers();


}
// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);

	//Definisco la matrice di Proiezione
	Projection = perspective(float(radians(45.0)), float(w) / float(h), 0.2f, 100.0f);

}



// Main routine.
int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("texture");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutIdleFunc(drawScene);


	glEnable(GL_DEPTH_TEST);
	glewExperimental = GL_TRUE;
	glewInit();
	costruisci_scena();
	INIT_VAO();
	genera_texture();
	glutMainLoop();
}


