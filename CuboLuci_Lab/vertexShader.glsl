#version 420 core

//Input vertex data, different for all execution of this shader
layout(location=0) in vec3 vertexPosition_modelSpace;
layout(location=1) in vec3 vertexColor;
layout(location=2) in vec3 vertexNormal;

struct Light{
vec4 LightPosition;
vec3 AmbientProduct;
vec3 DiffuseProduct;
vec3 SpecularProduct;
};

//constants
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform Light luce[2];		//We'll have 2 lights
uniform float Shiness;
uniform vec3 eye_position

//outputs
out vec3 fragmentColor;

void main(){

//Trasformo del coord del vetexin coord del world
	vec3 posM = Model*vec4(vertexPosition_modelSpace, 1).xyz;		//.xyz mi permette di prendere solo i primi 3 campi (su 4)
	vec3 N = normalize(Model*vec4(vertexNormal, 0));	//normal, 0 stands for vector
	vec3 V = normalize(eye_position - posM);

	fragmentColor = vec3(0.0);
	for(int i=0; i<2; i++){	//per ogni luce
		//calc light direcion
		vec3 L = normalize(luce[i].LightPosition.xyz - PosM).xyz;

		//ambient component
		vec3 ambient = luce[i].AmbientProduct;

		//diffuse component
		float cosT = max(dot(L,N), 0.0);	//dot: scalar product
		vec3 diffuse = luce[i].DiffuseProduct*cosT;

		//specular component
		vec3 R = -normalize(reflect(L, N));
		float cosA = max(dot(R,V), 0.0);	//dot: scalar product
		vec3 specular = luce[i].SpecularProduct*pow(cosA, Shiness);

		fragmentColor += ambient + diffuse + specular;	
	}

	gl_Position= Projection*View*Model*vec4(vertexPosition_modelSpace, 1);
}