﻿// CuboLuci.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"
#include "Lib.h"
#include "ShaderMaker.h"
#include "geometria.h"
#include "GeneraBuffer.h"


vec4 light1_position(2.0, 0.0, 0.0,1.0);
vec3 light1_ambient(1.0f, 1.0f, 1.0f);
vec3 light1_diffuse(0.7f, 0.7f, 0.7f);
vec3 light1_specular(1.0f,0.0f, 0.0f);

vec4 light2_position(0.0, 3.0, 0.0, 1.0);
vec3 light2_ambient(1.0f, 0.0f, 1.0f);
vec3 light2_diffuse(1.0f, 0.0f, 0.7f);
vec3 light2_specular(1.0f, 0.0f, 0.0f);

Light luci[2];
Light luce1;

//Informazioni sui materiali
vec3 material_ambient(0.1, 0.2, 0.2);
vec3 material_diffuse(0.4, 0.4, 1.0);
vec3 material_specular(1.0, 0.0, 0.0);
float material_shininess = 1000.0;
float angolox = 0, angoloy = 0, angoloz = 0;
vec3 ambient_product1 = light1_ambient * material_ambient;	//Ia * Ka
vec3 diffuse_product1 = light1_diffuse * material_diffuse;	//Id * Kd
vec3 specular_product1 = light1_specular * material_specular;	//Is * Ks

vec3 ambient_product2= light2_ambient * material_ambient;
vec3 diffuse_product2 = light2_diffuse * material_diffuse;
vec3 specular_product2 = light2_specular * material_specular;

void costruisci_luci(void)
{

	luce1.Lightposition = light1_position;
	luce1.AmbientProduct = ambient_product1;
	luce1.DiffuseProduct = diffuse_product1;
	luce1.SpecularProduct = specular_product1;

	Light luce2;
	luce2.Lightposition = light2_position;
	luce2.AmbientProduct = ambient_product2;
	luce2.DiffuseProduct = diffuse_product2;
	luce2.SpecularProduct = specular_product2;

	luci[0]=luce1;
	luci[1]=luce2;


}

GLuint MatrixModel, MatrixProj,la[2], ld[2], ls[2], lsh, lp[2], lO;

Point  *verticesC, *verticesP, *normaliC;
ColorRGBA *vertex_colorsC, *vertex_colorsP;
Indici *index_verticesC, *index_verticesP;
int frame;

GLuint programId;
//Strutture per la sfera
vector <Point> vertici_Sfera;
vector <ColorRGBA> colori_Sfera;
vector <GLuint> indici_Sfera;

mat4 Projection;
vec3 camera_pos(0, 0, 5), direzione(0.0, 0.0, -1.0), camera_up(0.0, 1.0, 0.0);

//Definizione dei VAO
GLuint VAO_CUBO;

//Locazione della matrice ModelViewProjection nello shader
GLuint MatrixID,MatrixView;

//PArametro telecamera
float angolo_rotY = -90.0f, angolo_rotX = 0.0f, lastX = 400.0, lastY = 400.0;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

vector<Light> luce;



GLfloat vertices[] = {
	.5f, .5f, .5f,  -.5f, .5f, .5f,  -.5f,-.5f, .5f,  .5f,-.5f, .5f,   // v0,v1,v2,v3 (front)
	.5f, .5f, .5f,   .5f,-.5f, .5f,   .5f,-.5f,-.5f,  .5f, .5f,-.5f,   // v0,v3,v4,v5 (right)
	.5f, .5f, .5f,   .5f, .5f,-.5f,  -.5f, .5f,-.5f, -.5f, .5f, .5f,   // v0,v5,v6,v1 (top)
	-.5f, .5f, .5f,  -.5f, .5f,-.5f,  -.5f,-.5f,-.5f, -.5f,-.5f, .5f,   // v1,v6,v7,v2 (left)
	-.5f,-.5f,-.5f,   .5f,-.5f,-.5f,   .5f,-.5f, .5f, -.5f,-.5f, .5f,   // v7,v4,v3,v2 (bottom)
	.5f,-.5f,-.5f,  -.5f,-.5f,-.5f,  -.5f, .5f,-.5f,  .5f, .5f,-.5f    // v4,v7,v6,v5 (back)
};

// normal array
GLfloat normals[] = {
	0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,  // v0,v1,v2,v3 (front)
	1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,  // v0,v3,v4,v5 (right)
	0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,  // v0,v5,v6,v1 (top)
	-1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  // v1,v6,v7,v2 (left)
	0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,  // v7,v4,v3,v2 (bottom)
	0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1   // v4,v7,v6,v5 (back)
};

// color array
GLfloat colors[] = {
	1, 1, 1,   1, 1, 0,   1, 0, 0,   1, 0, 1,  // v0,v1,v2,v3 (front)
	1, 1, 1,   1, 0, 1,   0, 0, 1,   0, 1, 1,  // v0,v3,v4,v5 (right)
	1, 1, 1,   0, 1, 1,   0, 1, 0,   1, 1, 0,  // v0,v5,v6,v1 (top)
	1, 1, 0,   0, 1, 0,   0, 0, 0,   1, 0, 0,  // v1,v6,v7,v2 (left)
	0, 0, 0,   0, 0, 1,   1, 0, 1,   1, 0, 0,  // v7,v4,v3,v2 (bottom)
	0, 0, 1,   0, 0, 0,   0, 1, 0,   0, 1, 1   // v4,v7,v6,v5 (back)
};


// index array for glDrawElements() ===========================================
// A cube has 36 indices = 6 sides * 2 tris * 3 verts
GLuint indices[] = {
	0, 1, 2,   2, 3, 0,    // v0-v1-v2, v2-v3-v0 (front)
	4, 5, 6,   6, 7, 4,    // v0-v3-v4, v4-v5-v0 (right)
	8, 9,10,  10,11, 8,    // v0-v5-v6, v6-v1-v0 (top)
	12,13,14,  14,15,12,    // v1-v6-v7, v7-v2-v1 (left)
	16,17,18,  18,19,16,    // v7-v4-v3, v3-v2-v7 (bottom)
	20,21,22,  22,23,20     // v4-v7-v6, v6-v5-v4 (back)
};


vec3 computeNormal(
	vec3 const & a,
	vec3 const & b,
	vec3 const & c)
{
	return glm::normalize(glm::cross(b - a, c - b));
}




// Initialization routine.
void INIT_VAO(void)
{
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	char* vertexShader = (char*)"vertexShader.glsl";
	char* fragmentShader = (char*)"fragmentShader.glsl";

	//Creo il programma in cui sono linkati i due shader compilati
	programId = ShaderMaker::createProgram(vertexShader, fragmentShader);

	//Lo utilizzo

	glUseProgram(programId);

	//Creao il VAO del cubo
	GLuint vertexbuffer, colorbuffer, indices_buffer, normalbuffer;

	glGenVertexArrays(1, &VAO_CUBO);
	glBindVertexArray(VAO_CUBO);

	//Cominciamo a Costruire i VBO di vertici, colori e indici dei vertici

	//Genero , rendo attivo, riempio ed assegno al layer 0 la geometria della mia primitva
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),vertices, GL_STATIC_DRAW);

	//Adesso carico il VBO dei vertici nel layer 0
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 


									  //Genero , rendo attivo, riempio ed assegno al layer 1 i colori di ogni vertice della primitiva
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	//Adesso carico il VBO del buffer dei colori nel layer 1
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 

	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 


									  //Genero un VBO per gli indici
	glGenBuffers(1, &indices_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}


void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	float currentFrame = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (currentFrame - lastFrame) / 1000;
	lastFrame = currentFrame;



	mat4 View;
	//Genero la matrice della Telecamera, (matrice di Vista)



	View = lookAt(camera_pos, camera_pos + direzione, camera_up);

	//Inizializzo all'identita' la trasformazione tipo Model (che mappa le coordinate dell'oggetto nel sistema di riferimento del mondo)

	mat4 Model;

	mat4 MVP;

	glBindVertexArray(VAO_CUBO);
	Model = mat4(1.0);

	Model = rotate(Model, radians(angolox), vec3(1.0, 1.0, 0.0));

	Model = rotate(Model, radians(angoloy), vec3(0.0, 1.0, 0.0));

		Model = rotate(Model, radians(angoloz), vec3(0.0, 0.0, 1.0));
		//La postmoltiplico per la matrice dei Scalatura
		Model = scale(Model, vec3(1.5, 1.5, 1.5));

		//Definisco la matrice ModelViewProjection
		mat4 MV = View * Model;
		
		//Assegno il valore attuale della matrice MV alla locazione MatrixID
		glUniformMatrix4fv(MatrixModel, 1, GL_FALSE, value_ptr(Model));
		glUniformMatrix4fv(MatrixView, 1, GL_FALSE, value_ptr(View));
		glUniformMatrix4fv(MatrixProj, 1, GL_FALSE, value_ptr(Projection));

		//Rendo attivo il VAO del cubo
		glDrawElements(GL_TRIANGLES, 24 * sizeof(GLuint), GL_UNSIGNED_INT, 0);

	//Rilascio il VABO
	glBindVertexArray(0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

	glutSwapBuffers();


}
// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);

	//Definisco la matrice di Proiezione
	Projection = perspective(float(radians(45.0)), float(w) / float(h), 0.1f, 100.0f);

}

void MousePassiveMotion(int x, int y)
{
	float xoffset, yoffset;

	//Memorizzo in xoffset la distanza tra l' ascissa della posizione attuale del mouse e quella precedente
	xoffset = x - lastX;
	//Memorizzo in yoffset la distanza tra l' ordinata della posizione attuale del mouse e quella precedente
	yoffset = y - lastY;

	float parametro_sensibilita_mouse = 0.005f;

	//Riduco la distanza in pixel in percentuale

	xoffset *= parametro_sensibilita_mouse;
	yoffset *= parametro_sensibilita_mouse;

	//Memorizzo  la posizione attuale del mouse
	lastX = x;
	lastY = y;

	//Incremento l'angolo di Yaw (ruotazione intorno all'asse y) di una quantit� pari all'incremento subito dalla posizione x del mouse 
	angolo_rotY += xoffset;
	//Incremento l'angolo di Pitch (ruotazione intorno all'asse x) di una quantit� pari all'incremento subito dalla posizione y del mouse 
	angolo_rotX += yoffset;

	//Ricavo la direzone corrispondente all'angolo di Yaw e Pitch aggiornati
	//(facendo uso delle coordinate sferiche)

	vec3 dir_temp;
	dir_temp.x = cos(radians(angolo_rotY))*cos(radians(angolo_rotX));
	dir_temp.y = sin(radians(angolo_rotX));
	dir_temp.z = sin(radians(angolo_rotY))*cos(radians(angolo_rotX));

	//Normalizzo la direzione
	direzione = normalize(dir_temp);
	glutPostRedisplay();

}
// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
/*	float cameraSpeed = deltaTime;

	//Sposto in avanti la camera nella direzione 
	if (key == 'w')
		camera_pos += cameraSpeed*direzione;

	//Sposto indietro la camera nella direzione 
	if (key == 's')
		camera_pos -= cameraSpeed*direzione;

	//Sposto a sinsitra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'a')
		camera_pos -= cameraSpeed*normalize(cross(direzione, camera_up));

	//Sposto a destra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'd')
		camera_pos += cameraSpeed*normalize(cross(direzione, camera_up));
		*/
	if (key == 'y')
		angoloy++;


	if (key == 'x')
		angolox++;


	if (key == 'z')
		angoloz++;

	glutPostRedisplay();
}



// Main routine.
int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Cubo Luci");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	//evento Mouse senza alcun tasto premuto

	//glutPassiveMotionFunc(MousePassiveMotion);

	glEnable(GL_DEPTH_TEST);

	glewExperimental = GL_TRUE;
	glewInit();
	costruisci_luci();
	
	INIT_VAO();



	MatrixModel = glGetUniformLocation(programId, "Model");
	MatrixView = glGetUniformLocation(programId, "View");
	MatrixProj = glGetUniformLocation(programId, "Projection");
	

	//chiedere l'indirizzo dell e variabili uniformi
	lp[0] = glGetUniformLocation(programId, "luce[0].LightPosition");
	la[0] = glGetUniformLocation(programId, "luce[0].AmbientProduct");
	ld[0] = glGetUniformLocation(programId, "luce[0].DiffuseProduct");
	ls[0] = glGetUniformLocation(programId, "luce[0].SpeculrProduct");

	lp[1] = glGetUniformLocation(programId, "luce[1].LightPosition");
	la[1] = glGetUniformLocation(programId, "luce[1].AmbientProduct");
	ld[1] = glGetUniformLocation(programId, "luce[1].DiffuseProduct");
	ls[1] = glGetUniformLocation(programId, "luce[1].SpeculrProduct");

	lsh = glGetUniformLocation(programId, "Shiness");
	lO = glGetUniformLocation(programId, "eye_position");

	//assegnare i valori alle var uniformi
	glUniform4f(lp[0], luci[0].Lightposition.x, luci[0].Lightposition.y, luci[0].Lightposition.z, luci[0].Lightposition.w);
	glUniform3f(la[0], luci[0].AmbientProduct.x, luci[0].AmbientProduct.y, luci[0].AmbientProduct.z);
	glUniform3f(ld[0], luci[0].DiffuseProduct.x, luci[0].DiffuseProduct.y, luci[0].DiffuseProduct.z);
	glUniform3f(ls[0], luci[0].SpecularProduct.x, luci[0].SpecularProduct.y, luci[0].SpecularProduct.z);

	glUniform4f(lp[1], luci[1].Lightposition.x, luci[1].Lightposition.y, luci[1].Lightposition.z, luci[0].Lightposition.w);
	glUniform3f(la[1], luci[1].AmbientProduct.x, luci[1].AmbientProduct.y, luci[1].AmbientProduct.z);
	glUniform3f(ld[1], luci[1].DiffuseProduct.x, luci[1].DiffuseProduct.y, luci[1].DiffuseProduct.z);
	glUniform3f(ls[1], luci[1].SpecularProduct.x, luci[1].SpecularProduct.y, luci[1].SpecularProduct.z);

	glUniform1f(lsh, material_shininess);
	glUniform3f(lO, camera_pos.x, camera_pos.y, camera_pos.z);




	glutMainLoop();
}




