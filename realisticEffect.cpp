﻿// Palla.cpp : definisce il punto di ingresso dell'applicazione console.
//
// ConsoleApplication1.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"
#include "geometry.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>

int width = 800; // dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonchè del mondo di gioco
int height = 800;
float raggiox = 50;
float raggioy = 50;
float x = width / 2;
float y = height / 2;

double static lerp(double a, double b, double amount) {
	return (1 - amount)*a + amount * b;
}

void disegna_palla(float x, float y, float raggiox, float raggioy) {
	ColorRGBA colore;
	ColorRGBA sfumatura;
	double shadow_scale = lerp(1, 0, raggiox / 200);

	//ombra
	//c_top = { 0, 0, 0, lerp(64,192,shadow_scale) };
	//c_bot = { 0, 0, 0, 0 };
	//draw_ellipse(x - bwidth / 2 * shadow_scale, y - 10 + 25 * (1 - shadow_scale), bwidth*shadow_scale, 50 * shadow_scale, c_top, c_bot);

	//ombra
	colore = { 0, 0, 0, 0.2 };
	sfumatura = { 0.0,0.0,0.0,0.0 };
	drawCircle(x, y - 50 * shadow_scale, raggiox, raggiox*shadow_scale, colore, sfumatura);

	//palla
	colore = { 1, 0, 0, 1 };
	sfumatura = { 1, 0.8, 0.8, 1 };
	drawCircle(x, y, raggiox, raggioy, colore, sfumatura);

	

	//speculare
	/*c_top = { 255, 255, 255, 192 };
	c_bot = { 255, 255, 255, 0 };
	draw_ellipse(x - bwidth / 3, y - bheight - height, bwidth / 1.5, bheight / 2, c_top, c_bot);*/
}

// Drawing routine.
void drawScene(void)
{

	//Definisce il colore con cui verr� ripulito lo schermo
	glClearColor(0.5, 1.0, 1.0, 0.0);

	//Pulisce lo schermo con il colore definito con la funzione glClearColor
	glClear(GL_COLOR_BUFFER_BIT);

	//Disegna prato
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);
	glColor3f(0.0, 1.0, 0.0);
	glVertex2f(0.0, 0.0);
	glColor3f(0.0, 1.0, 0.1);
	glVertex2f(width, 0.0);
	glColor3f(0.5, 1.0, 0.1);
	glVertex2f(width, height*0.7);
	glVertex2f(0, height*0.7);
	glEnd();



	disegna_palla(x, y, raggiox, raggioy);

	//Fa partire il processo di rendering
	glFlush();
}

// Initialization routine.
void setup(void)
{
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}




// Callback function per l'evento tastiera
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}



void  main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Prima Applicazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);


	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
}