﻿//comment the following line when compiling on linux
//#define WINDOWS
#ifdef WINDOWS
#include "stdafx.h"
#else
#include <stdio.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"
#include "geometry.h"
#include "movement.h"
#include "player.h"

#define UPPER_FLOOR_HEIGHT HEIGHT*0.9
#define LOWER_FLOOR_HEIGHT HEIGHT*0.7
#define UPPER_FLOOR_WIDTH WIDTH*0.1
#define LOWER_FLOOR_WIDTH WIDTH*0.15

static Player player;
ColorRGBA color1 = { 110,110,110,255 };
ColorRGBA color2 = { 184,184,184,255 };

/* Initial routine, called once, before the main loop. */
void setup(){
	player.direction = RIGHT;
	player.speed = initSpeed(0.0, 0.0);
	player.location.x = WIDTH / 2;
	player.location.y = HEIGHT / 2;
	Point2D bullets[] = {
		{0.0,0.0},
		{0.0,0.0},
		{0.0,0.0},
		{0.0,0.0}
	};
}

/* Drawing routine, called every loop (every FPS). */
void drawScene(void) {
	//Blank color
	glClearColor(0.8, 0.7, 0.0, 0.0);

	//Clean the view with the blank color
	glClear(GL_COLOR_BUFFER_BIT);

	/*
	//Draw the walls
	glColor3f(0.7, 0.5, 0.0);
	glBegin(GL_QUADS);
		glVertex2f(UPPER_FLOOR_WIDTH, 0.0);
		glVertex2f(LOWER_FLOOR_WIDTH, 0.0);
		glVertex2f(LOWER_FLOOR_WIDTH, LOWER_FLOOR_HEIGHT);
		glVertex2f(UPPER_FLOOR_WIDTH, UPPER_FLOOR_HEIGHT);
	glColor3f(0.6, 0.5, 0.0);
		glVertex2f(UPPER_FLOOR_WIDTH, UPPER_FLOOR_HEIGHT);
		glVertex2f(LOWER_FLOOR_WIDTH, LOWER_FLOOR_HEIGHT);
		glVertex2f(WIDTH - LOWER_FLOOR_WIDTH, LOWER_FLOOR_HEIGHT);
		glVertex2f(WIDTH - UPPER_FLOOR_WIDTH, UPPER_FLOOR_HEIGHT);
	glColor3f(0.7, 0.5, 0.0);
		glVertex2f(WIDTH - LOWER_FLOOR_WIDTH, LOWER_FLOOR_HEIGHT);
		glVertex2f(WIDTH - UPPER_FLOOR_WIDTH, UPPER_FLOOR_HEIGHT);
		glVertex2f(WIDTH - UPPER_FLOOR_WIDTH, 0.0);
		glVertex2f(WIDTH - LOWER_FLOOR_WIDTH, 0.0);

	glEnd();
	*/

	//Disegna player
	glPushMatrix();
	glTranslatef(player.location.x, player.location.y, 0);
	drawPlayerBody();
	glPushMatrix();
		glScalef(10, 10, 1);
		drawPlayerDetail(player.direction);
	glPopMatrix();
	glPopMatrix();

	//Fa partire il processo di rendering
	glutSwapBuffers();
}

/* OpenGL window reshape routine. */
void resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, WIDTH, 0.0, HEIGHT, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/* Handler for keyboard keys pression. */
void keyboardPressedEvent(unsigned char key, int x, int y) {
	switch (key) {
	case 'W':
	case 'w':
		moveUp(&player.speed);
		break;
		
	case 'A':
	case 'a':
		moveLeft(&player.speed);
		break;
		
	case 'S':
	case 's':
		moveDown(&player.speed);
		break;
		
	case 'D':
	case 'd':
		moveRight(&player.speed);
		break;

	case 27:
		exit(0);
		break;
		
	default:
		break;
	}
}

/* Handler for keyboard keys release. */
void keyboardReleasedEvent(unsigned char key, int x, int y) {
	switch (key) {
	case 'W':
	case 'w':
		stopUp(&player.speed);
		break;
		
	case 'A':
	case 'a':
		stopLeft(&player.speed);
		break;
		
	case 'S':
	case 's':
		stopDown(&player.speed);
		break;
		
	case 'D':
	case 'd':
		stopRight(&player.speed);
		break;
		
	case 27:
		exit(0);
		break;

	default:
		break;
	}
}

/* Handler for other special keys, such as the arrows. */
void arrowKeys(int key, int x, int y){
	switch(key){
	case GLUT_KEY_UP:
		player.direction = UP;
		shootUp(0);
		break;	

	case GLUT_KEY_LEFT:
		player.direction = LEFT;
		shootLeft(1);
		break;

	case GLUT_KEY_DOWN:
		player.direction = DOWN;
		shootDown(2);
		break;

	case GLUT_KEY_RIGHT:
		player.direction = RIGHT;
		shootRight(3);
		break;

	default:
		break;
	}
}

/* Animation routine. */
void update(int a) {
	//update player location
	player.location.x += player.speed.vx;
	player.location.y += player.speed.vy;
	//window border limits
	if(player.location.y>LOWER_FLOOR_HEIGHT){
		stopUp(&player.speed);
		player.location.y = LOWER_FLOOR_HEIGHT;
	}
	if(player.location.y<15){
		stopDown(&player.speed);
		player.location.y = 15;
	}
	if(player.location.x>WIDTH - LOWER_FLOOR_WIDTH - 10){
		stopRight(&player.speed);
		player.location.x = WIDTH - LOWER_FLOOR_WIDTH - 10;
	}
	if(player.location.x<LOWER_FLOOR_WIDTH + 10){
		stopLeft(&player.speed);
		player.location.x =LOWER_FLOOR_WIDTH + 10;
	}
	
	glutPostRedisplay();
	glutTimerFunc(11, update, 0);
}

