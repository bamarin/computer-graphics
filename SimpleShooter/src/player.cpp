#include <GL/glew.h>
#include <GL/freeglut.h>
#include "player.h"

static float posx_Proiettile_Mondo, posy_Proiettile_Mondo;
static ColorRGBA color_metaldark = { 110,110,110,255 };
static ColorRGBA color_metallight = { 184,184,184,255 };
static ColorRGBA color_cabindark = { 66,134,244,255 };
static ColorRGBA color_cabinlight = { 158,195,255,255 };
Point2D bullets[4];

/* Move the projectile up untill it gets too far.
 * Then place it place it back on the player.
 */
void shootUp(int i) {
	bullets[i].x = 0;
	bullets[i].y++;

	if (bullets[i].y <= 10){
		glutTimerFunc(5, shootUp, 0);
	}
	else
		bullets[i].y = 0;
}

/* Move the projectile left untill it gets too far.
 * Then place it place it back on the player.
 */
void shootLeft(int i) {
	bullets[i].y = 0;
	bullets[i].x--;

	if (bullets[i].x >= -10)
		glutTimerFunc(5, shootLeft, 1);
	else
		bullets[i].x = 0;
}

/* Move the projectile down untill it gets too far.
 * Then place it place it back on the player.
 */
void shootDown(int i) {
	bullets[i].x = 0;
	bullets[i].y--;

	if (bullets[i].y >= -10)
		glutTimerFunc(5, shootDown, 2);
	else
		bullets[i].y = 0;
}

/* Move the projectile right untill it gets too far.
 * Then place it place it back on the player.
 */
void shootRight(int i) {
	bullets[i].y = 0;
	bullets[i].x++;

	if (bullets[i].x <= 10)
		glutTimerFunc(5, shootRight, 3);
	else
		bullets[i].x = 0;
}

void drawPlayerDetail(Direction d){
	switch(d){
	case UP:
			glPushMatrix();
				//piedi
				glScalef(0.5, 0.5, 1);
				glPushMatrix();
					//sx
					glTranslatef(-1, -3, 0);
					drawHalfCircle(color_metaldark, color_metallight);
				glPopMatrix();
				glPushMatrix();
					//dx
					glTranslatef(1, -3, 0);
					drawHalfCircle(color_metaldark, color_metallight);
				glPopMatrix();
			glPopMatrix();
		break;
		
	case DOWN:
			glPushMatrix();
				//visiera
				glTranslatef(0, 1.5, 0);
				glScalef(0.7, 0.6, 1);
				glRotatef(90,0,0,1);
				drawMoon({0,0,0,255}, {0,0,0,255});
			glPopMatrix();
			glPushMatrix();
				//piedi
				glScalef(0.5, 0.5, 1);
				glPushMatrix();
					//sx
					glTranslatef(-1, -3, 0);
					drawHalfCircle(color_metaldark, color_metallight);
				glPopMatrix();
				glPushMatrix();
					//dx
					glTranslatef(1, -3, 0);
					drawHalfCircle(color_metaldark, color_metallight);
				glPopMatrix();
			glPopMatrix();
		break;
		
	case LEFT:
		glPushMatrix();
			glScalef(-1, 1, 1);
			glPushMatrix();
				//visiera
				glTranslatef(0, 2.3, 0);
				glScalef(2.3, -2.3, 1);
				drawSector({0,0,0,255}, {0,0,0,255});
			glPopMatrix();
			glPushMatrix();
				//piedi
				glScalef(0.5, 0.5, 1);
				glTranslatef(0, -3, 0);
				for(int i=0; i<2; i++)
					drawHalfCircle(color_metaldark, color_metallight);
			glPopMatrix();
		glPopMatrix();
		break;
		
	case RIGHT:
			glPushMatrix();
				//visiera
				glTranslatef(0, 2.3, 0);
				glScalef(2.3, -2.3, 1);
				drawSector({0,0,0,255}, {0,0,0,255});
			glPopMatrix();
			glPushMatrix();
				//piedi
				glScalef(0.5, 0.5, 1);
				glTranslatef(0, -3, 0);
				for(int i=0; i<2; i++)
					drawHalfCircle(color_metaldark, color_metallight);
			glPopMatrix();
		break;
		
	default:
		break;
	}

}

void drawPlayerBody(){
	glPushMatrix();
	glScalef(10, 10, 1);
	//Disegna 4 Proiettili
	glPushMatrix();
	glScalef(10, 10, 1);
	glColor3d(0,0,0);
	glPointSize(10.0);
	for(int i=0; i<4; i++){
		glPushMatrix();
		glTranslatef(bullets[i].x, bullets[i].y, 0.0);
		glBegin(GL_POINTS);
		glVertex3f(0.0, 0, 0);
		glEnd();

		//Matrice di ModelView che agisce sul proiettile
		float matrix[16];
		glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

		float c_p_O[4];	//coord omogenee del proiettile
		c_p_O[0] = 0;
		c_p_O[1] = 0;
		c_p_O[2] = 0;
		c_p_O[3] = 1;

		//Calcolare le coordinate assunte dal proiettile nel mondo
		float c_p_M[4];
		for (int i = 0; i < 4; i++) {
			c_p_M[i] = 0;
			for (int j = 0; j < 4; j++) {
				c_p_M[i] += matrix[j * 4 + i] + c_p_O[j];
			}
		}

		posx_Proiettile_Mondo = c_p_M[0];
		posy_Proiettile_Mondo = c_p_M[1];

		glPopMatrix();
	}
	glPopMatrix();
	//Disegna corpo
	drawCircle(color_metaldark, color_metallight);
	//Disegna testa
	glPushMatrix();
	glTranslatef(0, 2, 0);
	glScalef(2, 2, 1);
	drawCircle(color_metaldark, color_metallight);
	glPopMatrix();
	glPopMatrix();

}

