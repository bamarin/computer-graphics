//comment the following command when compiling on linux
//#define WINDOWS
#ifdef WINDOWS
#include "stdafx.h"
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "movement.h"

/* Utility variables needed in order to know if there are multiple keys pressed at the same moment.
 * These allow us to set the moving direction to the former one if a key was released but other are
 * still pressed.
 *
 * e.g.
 * A is pressed      -> player starts moving to the left
 * D is aslo pressed -> player starts moving to the right
 * D is released     -> player continues moving to the left
 * n.b. the A key wasn't released
 */
static bool pressingW = false;
static bool pressingA = false;
static bool pressingS = false;
static bool pressingD = false;

/* Init the speed */
Speed initSpeed(float xVelocty, float yVelocty){
	Speed speed = {xVelocty, yVelocty};
	return speed;
}

/* Set the vertical speed to positive.
 */
void moveUp(Speed* speed){
	pressingW = true;
	speed->vy = +SPEED;
}

/* Set the orizontal speed to negative.
 */
void moveLeft(Speed* speed){
	pressingA = true;
	speed->vx = -SPEED;
}

/* Set the vertical speed to negative.
 */
void moveDown(Speed* speed){
	pressingS = true;
	speed->vy = -SPEED;
}

/* Set the orizontal speed to positive.
 */
void moveRight(Speed* speed){
	pressingD = true;
	speed->vx = +SPEED;
}

/* Prevents the entity from going further up.
 */
void stopUp(Speed* speed){
	pressingW = false;
	if(pressingS)
		moveDown(speed);
	else
		speed->vy = 0;
}

/* Prevents the entity from going further left.
 */
void stopLeft(Speed* speed){
	pressingA = false;
	if(pressingD)
		moveRight(speed);
	else
		speed->vx = 0;
}

/* Prevents the entity from going further down.
 */
void stopDown(Speed* speed){
	pressingS = false;
	if(pressingW)
		moveUp(speed);
	else
		speed->vy = 0;
}

/* Prevents the entity from going further right.
 */
void stopRight(Speed* speed){
	pressingD = false;
	if(pressingA)
		moveLeft(speed);
	else
		speed->vx = 0;
}

/* Returns true if the player is moving.
 * Returns false if the player is standing.
 */
bool isMoving(Speed speed){
	if(speed.vx == 0 && speed.vy == 0)
		return false;
	return true;
}
