//comment the following command when compiling on linux
//#define WINDOWS
#ifdef WINDOWS
#include "stdafx.h"
#else
#include <stdio.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "geometry.h"

void drawCircle(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES; i++)
		/* circle vertices */
		glVertex2f(cos((double)i *stepA), sin((double)i *stepA));
	glEnd();
}

void drawHalfCircle(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES/2; i++)
		/* circle vertices */
		glVertex2f(cos((double)i *stepA), sin((double)i *stepA));
	glEnd();
}

void drawSector(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES/8; i++)
		/* circle vertices */
		glVertex2f(cos((double)i *stepA), sin((double)i *stepA));
	glEnd();
}

void drawMoon(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES; i++)
		/* circle vertices */
		glVertex2f(
				0.5-cos(2*((double)i *stepA))-cos((double)i *stepA),
				3*sin((double)i *stepA));
	glEnd();
}

double static lerp(double a, double b, double amount) {
	return (1 - amount)*a + amount * b;
}

void drawEllipse(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES; i++)
		/* circle vertices */
		glVertex2f(2*cos((double)i *stepA), sin((double)i *stepA));
	glEnd();
}

void drawHeart(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES; i++)
		/* circle vertices */
		glVertex2f(
				13*cos((double)i *stepA)-5*cos(2*((double)i *stepA))-2*cos(3*((double)i *stepA))-cos(4*((double)i *stepA)),
				16*pow(sin((double)i *stepA),3)
				);
	glEnd();
}

void drawButterfly(ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / CIRLCE_VERTICES;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	/*center of the circle*/
	glVertex2f(0, 0);
	glColor4d(colore.r / 255, colore.g / 255, colore.b / 255, colore.a / 255);
	for (i = 0; i <= CIRLCE_VERTICES; i++)
		/* circle vertices */
		glVertex2f(
				sin((double)i *stepA)*exp(cos((double)i *stepA)-2*cos(4*((double)i *stepA))-pow(sin(((double)i *stepA)/2),5)),
				cos((double)i *stepA)*exp(cos((double)i *stepA)-2*cos(4*((double)i *stepA))-pow(sin(((double)i *stepA)/2),5))
				);
	glEnd();
}

