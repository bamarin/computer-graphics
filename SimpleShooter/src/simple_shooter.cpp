﻿/* Author: Leonardo Marini
 * Project1: Simple GL animation
 * Name: Simple Shooter
 * Computer Graphics
 * A.A.: 2018/2019
 */

//comment the following line when compiling on linux
//#define WINDOWS
#ifdef WINDOWS
#include "stdafx.h"
#else
#include <stdio.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"
#include "geometry.h"

int main(int argc, char **argv)
{
	if(argc != 3){
		fprintf(
			stdout,
			"ERROR: specify OpenGL version in separate numbers, for example\n%s 3 0 (for GL version\
3.0)\n\nTo know what version you are currently using run:\nglxinfo", argv[0]
		);
		exit(EXIT_FAILURE);
		return 1;
	}

	GLboolean GlewInitResult;

	//Init Toolkit
	glutInit(&argc, argv);
	//Init Context
	glutInitContextVersion(atoi(argv[1]), atoi(argv[2]));
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	//Init Window
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Simple Shooter");
	//main routines
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	//inputs
	glutKeyboardFunc(keyboardPressedEvent);
	glutKeyboardUpFunc(keyboardReleasedEvent);
	glutSpecialFunc(arrowKeys);

	//setup
	setup();
	//animation events
	glutTimerFunc(11, update, 0);

	GlewInitResult = glewInit();
	if(GlewInitResult != GLEW_OK){
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
		return 1;
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
	return 0;
}
