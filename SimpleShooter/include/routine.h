#pragma once

/* Canvas size, corresponding to the initial window size and the game world. */
#define WIDTH 800
#define HEIGHT 600

/* Initial routine, called once, before the main loop. */
void setup();

/* Drawing routine, called every loop (every FPS). */
void drawScene(void);

/* OpenGL window reshape routine. */
void resize(int w, int h);

/* Handler for keyboard keys pression. */
void keyboardPressedEvent(unsigned char key, int x, int y);

/* Handler for keyboard keys release. */
void keyboardReleasedEvent(unsigned char key, int x, int y);

/* Handler for other special keys, such as the arrows. */
void arrowKeys(int key, int x, int y);

/* Animation routine. */
void update(int a);

