#pragma once

#define PI 3.14159265358979323846
#define CIRLCE_VERTICES 24

/// Defines an RGBA color by setting each field within a range (0-255)
typedef struct { double r, g, b, a; } ColorRGBA;

typedef struct {
	float x;
	float y;
}Point2D;


/// Draws an ellipse given its center(x,y) radius(rx,ry) and a gradient of 2 colors (the second for the center)
void drawCircle(ColorRGBA colore, ColorRGBA sfumatura);	// if rx==ry it draws a circle
void drawHalfCircle(ColorRGBA colore, ColorRGBA sfumatura);
void drawSector(ColorRGBA colore, ColorRGBA sfumatura);
void drawEllipse(ColorRGBA colore, ColorRGBA sfumatura);
void drawMoon(ColorRGBA colore, ColorRGBA sfumatura);
void drawHeart(ColorRGBA colore, ColorRGBA sfumatura);
void drawButterfly(ColorRGBA colore, ColorRGBA sfumatura);
void disegnaUfo();

/// Fades 
double static lerp(double a, double b, double amount);

