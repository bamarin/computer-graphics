#pragma once

#include "geometry.h"
#include "movement.h"

typedef struct {
	Point2D location;
	Speed speed;
	Direction direction;
} Player;
extern Point2D bullets[4];

/* Move the projectile up untill it gets too far.
 * Then place it place it back on the player.
 */
void shootUp(int value);

/* Move the projectile left untill it gets too far.
 * Then place it place it back on the player.
 */
void shootLeft(int value);

/* Move the projectile down untill it gets too far.
 * Then place it place it back on the player.
 */
void shootDown(int value);

/* Move the projectile right untill it gets too far.
 * Then place it place it back on the player.
 */
void shootRight(int value);

void drawPlayer();
void drawPlayerBody();
void drawPlayerDetail(Direction d);
