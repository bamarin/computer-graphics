#pragma once

#define SPEED 5

typedef enum {UP,LEFT,DOWN,RIGHT} Direction;
typedef struct{
	float vx;
	float vy;
}Speed;

/* Init the speed */
Speed initSpeed(float xVelocty, float yVelocty);

/* Internal method that manages combination of multiple directions sequences. */
void move(Speed* speed);

/* Set the vertical speed to positive.
 */
void moveUp(Speed* speed);

/* Set the orizontal speed to negative.
 */
void moveLeft(Speed* speed);

/* Set the vertical speed to negative.
 */
void moveDown(Speed* speed);

/* Set the orizontal speed to positive.
 */
void moveRight(Speed* speed);

/* Prevents the entity from going further up.
 */
void stopUp(Speed* speed);

/* Prevents the entity from going further left.
 */
void stopLeft(Speed* speed);

/* Prevents the entity from going further down.
 */
void stopDown(Speed* speed);

/* Prevents the entity from going further right.
 */
void stopRight(Speed* speed);

/* Returns true if the player is moving.
 * Returns false if the player is standing.
 */
bool isMoving(Speed speed);
