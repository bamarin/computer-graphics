#version 420 core

in vec3 Color;
in vec3 coord;

out vec4 Color_interpolato;

//void main (){
//vec3 col1 = vec3(0.0,0.0,0.8);
//vec3 col2 = vec3(0.8,0.0,0.0);
//float value = 0.5 * (1.0+(sin(coord.x*0.5)*sin(coord.z*2.0)));
//vec3 color = mix(col1, col2, value);
//Color_interpolato = vec4(color, 1.0);
//}

void main (){
float scale = 20.0 / 800.0;
float fr = fract(gl_FragCoord.x * scale);
//Color_interpolato = vec4(step(0.5, fr), 0.0,0.0,1.0);
Color_interpolato = vec4(smoothstep(0.2,0.8, fr), 0.0,0.0,1.0);

}
