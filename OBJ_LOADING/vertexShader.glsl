#version 420 core

layout(location=0) in vec3 vertexPosition_modelSpace;

uniform mat4 MV;

out vec3 Color;

void main(){

	gl_Position=MV*vec4(vertexPosition_modelSpace,1);

	Color=vec3(1.0,0.0,0.0);
}