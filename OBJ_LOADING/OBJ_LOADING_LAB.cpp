// OBJ_LOADING.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "ShaderMaker.h"

// Include GLEW
#include <GL/glew.h>
#include <GL\freeglut.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

//#include "shader.h"
using namespace glm;

float fov = 45.0f;
int larghezza = 800,ww;
int altezza = 800,hh;
#include "objloader.hpp"

using namespace std;
GLuint programId;
float angolox=0.0, angoloy=0.0, angoloz=0.0, zcam = 8;
std::vector<glm::vec3> vertices;
std::vector <GLuint> index_vertices,normalIndices;
std::vector<glm::vec3> normals; 
mat4 view, Projection, model,MVP;
GLuint MatrixID, ModelMatrixID, ViewMatrixID;
glm::vec3 lightPos = glm::vec3(0, 4, 0);
GLuint LightID;
GLuint VAO_MODELLO;

GLuint crea_VAO_Vector(GLuint VAO, vector <vec3> vertici, vector<GLuint> indici)
{
	GLuint vertexbuffer, colorbuffer, elements_buffer;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertici.size() * sizeof(vec3), &vertici[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0, // attributo
		3,                 // numero di elementi per vertice,  (x,y,z)
		GL_FLOAT,          // il tipo di ogni elemento
		GL_FALSE,          // non normalizza il vaore
		0,                 // non ci sondati extra tra ogni posizione 
		0                  // offset del primo elemento
	);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//VBO di tipo indici
	glGenBuffers(1, &elements_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indici.size() * sizeof(GLuint), &indici[0], GL_STATIC_DRAW);
	return VAO;

}



void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	 view = lookAt(vec3(0.0, 0.0,zcam), vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
	 Projection = glm::perspective(radians(fov),(float)( ww)/(float)( hh), 0.1f, 100.0f);
	 model = mat4(1.0);
	 model = rotate(model, radians(angolox), vec3(1.0, 0.0, 0.0));
	 model = rotate(model, radians(angoloy), vec3(0.0, 1.0, 1.0));

	 MVP = Projection * view * model;

	 MatrixID = glGetUniformLocation(programId, "MV");

	 // Carica in VBO

	 glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	 
	
	 glBindVertexArray(VAO_MODELLO);
		glDrawElements(GL_TRIANGLES, index_vertices.size()*sizeof(GLuint),GL_UNSIGNED_INT,0);
		glBindVertexArray(0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glutSwapBuffers();

}
// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{

	
	case 'x':
		angolox += 5;
		//printf("angolo %f \n", angolox);
		break;
	case 'y':
		angoloy += 5;
		//printf("angolo %f \n", angoloy);
		break;
	case 'z':
		angoloz += 5;
		//printf("angolo %f \n", angoloz);
		break;
	case 'T':
		zcam += 0.1;
		//printf("zcam %f \n", zcam);
		break;
	case 't':
		zcam -= 0.1;
		//printf("angolo %f \n", zcam);
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	ww = w;
	hh = h;
}

void mouse_rotella(int wheel, int direction, int x, int y) {
	(direction > 0) ? fov++ : fov--;
	glutPostRedisplay();
}

void INIT_VAO(void)
{
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	char* vertexShader = (char*)"vertexShader.glsl";
	char* fragmentShader = (char*)"fragmentShader.glsl";

	programId = ShaderMaker::createProgram(vertexShader, fragmentShader);
	glUseProgram(programId);

	bool leggiObj = loadOBJ("shuttle.obj", &vertices, &index_vertices, &normals, &normalIndices);
	//Creao il VAO
	VAO_MODELLO = crea_VAO_Vector(VAO_MODELLO, vertices, index_vertices);


}

void main(int argc, char **argv)
{

	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
	glutInitWindowSize(larghezza, altezza);
	glutInitWindowPosition(0, 0);
	 glutCreateWindow("Visualizzazione OBJ Models");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	glutMouseWheelFunc(mouse_rotella);

	
	glewExperimental = GL_TRUE;
	glewInit();
	INIT_VAO();
	
	glutMainLoop();

}

