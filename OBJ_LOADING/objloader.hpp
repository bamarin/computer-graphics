#ifndef OBJLOADER_H
#define OBJLOADER_H
#include <GL/glew.h>
#include <GL\freeglut.h>


bool loadOBJ(
	const char * path,
	std::vector<glm::vec3> * vertices,
	std::vector <GLuint> *vertexIndices,
	std::vector<glm::vec3> * normals,
	std::vector <GLuint> *normalIndices
);

//std::vector<glm::vec3>  normalizzaObj(std::vector<glm::vec3> vertices);


#endif