# Computer Graphics

## requirements (dependencies)

Install the following packets:

* glew
* freeglut
* glxinfo (for troubleshooting)
* git

## to run the mini game I made:

1. clone the repo `git clone https://leonardo9067@bitbucket.org/bamarin/computer-graphics.git`
2. switch to branch develop `git checkout develop`
2. change directory to SimpleSchooter `cd SimpleShooter`
3. run `make`
4. run the game followed by the installed OpenGL version (space separated instaed of dots)
e.g. `./bin/simpleShooter 3 0` (for GL version 3.0)

If you don't know the installed openGL version run `glxinfo` and search for OpenGL Version in the output