// sierpinski.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>

#include <vector>
using namespace std;

typedef struct { float x; float y; }POINT2D;
//Variabili globali
vector <POINT2D> Punti;

int width = 800; // dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonch� del mondo di gioco
int height = 800;
float gb = 0;
float delta = 0.01;

void sierpinski(void)
{
	typedef struct { int x, y; } Point;
	Point T[3] = { { 100,100 } ,{ 700,100 } ,{ 400,700 } };
	int i, index = rand() % 3;
	// Scegliamo il punto iniziale	p0		, a caso tra uno dei tre		vertici
		Point point = T[index];
	//Disegna il punto iniziale
	glPointSize(1.0);
	glBegin(GL_POINTS);
	glVertex3f(point.x, point.y, 0);
	glEnd();
	for (i = 0; i<50; i++)
	{
		//Scelgo a caso uno dei tre vertici iniziali
		index = rand() % 3;
		//Il nuovo punto � il punto medio tra il punto precedente
		//ed il vertice scelto a caso.
		point.x = (point.x + T[index].x) / 2;
		point.y = (point.y + T[index].y) / 2;
		glBegin(GL_POINTS);
		glColor3d(rand() % 2, rand() % 2, rand() % 2);
		glVertex3f(point.x, point.y, 0);
		glEnd();
	}
	glFlush();
}

void setup() {
	//Definisce il colore con cui verr� ripulito lo schermo
	glClearColor(0, 0, 0, 0);

}

// Drawing routine.
void drawScene(void)
{


	//glColor3f(1, 0, 0);
	//glEnable(GL_LINE_STIPPLE);
	//glLineStipple(4, 0x00cf);
	//glBegin(GL_LINE_STRIP);
	//glVertex2d(0, 0);
	//glVertex2d(width, height);
	//glEnd();
	//glDisable(GL_LINE_STIPPLE);
	//glFlush();
	//
	//sierpinski();
	//
	//glPointSize(10.0);

	int i;
	glClearColor(0, gb, 0, 0);

	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(5.0);
	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	for (i = 0; i<Punti.size(); i++)
		glVertex2f(Punti.at(i).x, Punti.at(i).y);
	glEnd();
	//abilito la modalit� di disegno primitive in maniera tratteggiata
	/*	glEnable(GL_LINE_STIPPLE);
	glLineStipple(4, 0x3F07);
	glBegin(GL_LINE_STRIP);
	for (i = 0; i<Punti.size(); i++)
		glVertex2f(Punti.at(i).x, Punti.at(i).y);
	glEnd();
	//Disabilito la modalit� di disegno primitive in maniera tratteggiata
	glDisable(GL_LINE_STIPPLE);
	*/
	//Faccio partire la pipeline grafica

	glFlush();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Callback function per l'evento tastiera
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'e':
	case 'E':
	case 27:
		exit(0);
		break;
	case 'c':
	case 'C':
		glClearColor(1, 1, 1, 0);
		break;
	case 'r':
	case 'R':
		setup();
		break;
	default:
		break;
	}
	//force to redraw the scene at any keyboard event
	glutPostRedisplay();
}

void myMouse(int button, int state, GLint xmouse, GLint
	ymouse)
{
	int i;
	POINT2D newPoint;
	//Memorizzo in newPoint le coordinate in cui si trova il mouse quando viene fatto click
	//In OpenGL, le ordinate del mouse sono misurate a partire dal corner pi� in alto a
	//sinistra.Per avere il punto disegnato nella posizione effettiva in cui si fa click,
	//bisogna sottrarre l'ordinata del mouse dall'altezza della viewport sullo schermo.
	newPoint.x = xmouse;
	newPoint.y = height - ymouse;
	if (state == GLUT_DOWN)
	{
		switch (button)
		{
		case GLUT_LEFT_BUTTON:
			Punti.push_back(newPoint);
			break;
		case GLUT_RIGHT_BUTTON:
			Punti.pop_back();
			break;
		}
	}
	glutPostRedisplay();
}

void mouseMotion(int x, int y)
{
	POINT2D newPoint;
	newPoint.x = x;
	newPoint.y = height - y;
	Punti.push_back(newPoint);
	glutPostRedisplay();
}
void update(int value)
{
	/*Per avere gradualmente tutte le
	sfumature dal blu al ciano basta tenere
	fisso r=0 e b=1, e fare variare g da 0 a
	1 gradualmente*/
	if (gb <0 || gb > 1)
		delta = -delta;
	gb += delta;
	glutPostRedisplay();
	glutTimerFunc(50, update, 0);
}

void  main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Prima Applicazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutMouseFunc(myMouse);
	glutMotionFunc(mouseMotion);
	glutTimerFunc(50, update, 0);



	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);
	setup();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
}

