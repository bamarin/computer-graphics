﻿// simple_shooter.cpp : definisce il punto di ingresso dell'applicazione console.

#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"
#include "geometry.h"

void  main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	//Init Toolkit
	glutInit(&argc, argv);
	//Init Context
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	//Init Window
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Prima Applicazione");
	//main routines
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	//inputs
	glutKeyboardFunc(keyboardPressedEvent);
	glutKeyboardUpFunc(keyboardReleasedEvent);
	//animations events
	glutTimerFunc(11, update, 0);



	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
}