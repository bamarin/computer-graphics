#pragma once

#define SPEED 5

/// Increase the vertical speed with a positive accelleration
/// a: accelleration
void moveUp(float a);
/// Increase the orizontal speed with a negative accelleration
/// a: accelleration
void moveLeft(float a);
/// Increase the vertical speed with a negative accelleration
/// a: accelleration
void moveDown(float a);
/// Increase the orizontal speed with a positive accelleration
/// a: accelleration
void moveRight(float a);
/// Decrease the speed down to 0
void decelerate();
