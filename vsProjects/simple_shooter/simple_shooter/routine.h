#pragma once

// dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonch� del mondo di gioco
#define WIDTH 800
#define HEIGHT 800

/// Drawing routine.
void drawScene(void);

/// OpenGL window reshape routine.
void resize(int w, int h);

/// Function called when a keyboard key is pressed
void keyboardPressedEvent(unsigned char key, int x, int y);

/// Function called when a keyboard key is released
void keyboardReleasedEvent(unsigned char key, int x, int y);

/// Animation routine
void update(int a);
