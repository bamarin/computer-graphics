#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "geometry.h"

void drawCircle(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	double stepA = (2 * PI) / CIRLCE_VERTICES;
	glBegin(GL_TRIANGLE_FAN);
	/*center of the circle*/
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	glVertex2f(x, y);
	/* circle vertices */
	for (i = 0; i <= CIRLCE_VERTICES; i++)
	{
		glColor4d(colore.r, colore.g, colore.b, colore.a);
		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
	}
	glEnd();
}

double static lerp(double a, double b, double amount) {
	return (1 - amount)*a + amount * b;
}

