﻿#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"
#include "geometry.h"
#include "movement.h"

// Drawing routine.
void drawScene(void) {
	float raggiox = 50;
	float raggioy = 50;
	float x = WIDTH / 2;
	float y = HEIGHT / 2;
	//Blank color
	glClearColor(0.5, 1.0, 1.0, 0.0);

	//Clean the view with the blank color
	glClear(GL_COLOR_BUFFER_BIT);

	//Draw the grass
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);
		glColor3f(0.0, 1.0, 0.0);
		glVertex2f(0.0, 0.0);

		glColor3f(0.0, 1.0, 0.1);
		glVertex2f(WIDTH, 0.0);

		glColor3f(0.5, 1.0, 0.1);
		glVertex2f(WIDTH, HEIGHT*0.7);
		glVertex2f(0, HEIGHT*0.7);
	glEnd();



	//disegna_palla(x, y, raggiox, raggioy);
	ColorRGBA colore = { 1, 0, 0, 1 };
	ColorRGBA sfumatura = { 1, 1, 1, 255 };
	drawCircle(x, y, raggiox, raggioy, colore, sfumatura);

	//Fa partire il processo di rendering
	glFlush();
}

// OpenGL window reshape routine.
void resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, WIDTH, 0.0, HEIGHT, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Function called when a keyboard key is pressed
void keyboardPressedEvent(unsigned char key, int x, int y) {
	switch (key) {
	case 'W':
	case 'w':
		moveUp();
		break;
	case 'A':
	case 'a':
		moveLeft();
		break;
	case 'S':
	case 's':
		moveDown();
		break;
	case 'D':
	case 'd':
		moveRight();
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

// Function called when a keyboard key is pressed
void keyboardReleasedEvent(unsigned char key, int x, int y) {
	switch (key) {
	case 'W':
	case 'w':
	case 'A':
	case 'a':
	case 'S':
	case 's':
	case 'D':
	case 'd':
		decelerate();
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

void update(int a) {}
//	bool moving = false;
//	//Movimento della navicella in orizzontale
//
//	frame_animazione++;
//	if (frame_animazione >= 360) {
//		frame_animazione -= 360;
//	}
//
//	if (pressing_left)
//	{
//		VelocitaOrizzontale -= accelerazione;
//		moving = true;
//	}
//
//	if (pressing_right)
//	{
//		VelocitaOrizzontale += accelerazione;
//		moving = true;
//	}
//
//
//
//	if (!moving) {   //Se non mi sto muovendo con i tasti a e d decremento od incremento la velocita' iniziale fino a portarla
//					 // a zero e l'ufo continua a roteare sul posto
//
//		if (VelocitaOrizzontale > 0)
//		{
//			VelocitaOrizzontale -= 1;
//			if (VelocitaOrizzontale < 0)
//				VelocitaOrizzontale = 0;
//		}
//
//		if (VelocitaOrizzontale < 0)
//		{
//			VelocitaOrizzontale += 1;
//			if (VelocitaOrizzontale > 0)
//				VelocitaOrizzontale = 0;
//		}
//	}
//
//
//
//	//Aggioramento della posizione in x dell'ufo, che regola il movimento orizzontale
//
//	posx += VelocitaOrizzontale;
//
//
//
//	//Se l'ufo  assume una posizione in x minore di 0 o maggiore di width dello schermo
//	//facciamo ritornare indietro l'ufo
//
//	if (posx < 0) {
//		posx = 0;
//		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
//	}
//
//	if (posx >width) {
//		posx = width;
//		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
//	}
//
//	posy = balance_position + sin(degtorad(frame_animazione))*floating_range;
//	FloatingAngle = cos(degtorad(frame_animazione))*angle_offset - VelocitaOrizzontale*1.3;
//
//	glutPostRedisplay();
//	glutTimerFunc(15, update, 0);
//}