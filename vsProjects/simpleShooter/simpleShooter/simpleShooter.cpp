#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "stdafx.h"

void drawScene() {
	//background color
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//main color
	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(10.0f); // Definisce la dimensione del punto

	glBegin(GL_POLYGON);
	glVertex2f(20.0, 20.0);
	glVertex2f(80.0, 20.0);
	glVertex2f(80.0, 80.0);
	glVertex2f(20.0, 80.0);
	glEnd();
	//Fa partire il processo di rendering
	glFlush();
}

// OpenGL window reshape routine.
void resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char **argv) {

	GLboolean GlewInitResult;

	//Init Toolkit
	glutInit(&argc, argv);
	//Init Context
	glutInitContextVersion(3, 0);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitContextFlags(GLUT_DEBUG);
	//Init Window
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("Test");
	//main functions
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);

	//Check Init errors
	GlewInitResult = glewInit();
	if (GlewInitResult != GLEW_OK) {
		fprintf(stderr, "ERROR: %s \n", glewGetErrorString(GlewInitResult));
		exit(EXIT_FAILURE);
	}

	//Check OpenGl version
	fprintf(stdout, "INFO: OpenGl Version: %s\n", glGetString(GL_VERSION));

	//keep the window open
	glutMainLoop();
}
