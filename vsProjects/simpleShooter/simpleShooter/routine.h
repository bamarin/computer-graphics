#include "stdafx.h"

#ifndef ROUTINE
#define ROUTINE

/// Drawing routine.
void drawScene(void);

/// OpenGL window reshape routine.
void resize(int w, int h);

#endif //ROUTINE