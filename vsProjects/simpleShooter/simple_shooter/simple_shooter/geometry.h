#pragma once

#define PI 3.14159265358979323846
#define nvertices 24

/// Defines an RGBA color by setting each field within a range (0-255)
typedef struct { double r, g, b, a; } ColorRGBA;

/// Draws an ellipse given its center(x,y) radius(rx,ry) and a gradient of 2 colors (the second for the center)
void disegna_cerchio(float x, float y, float rx, float ry, ColorRGBA pimario, ColorRGBA secondario);	// if rx==ry it draws a circle

