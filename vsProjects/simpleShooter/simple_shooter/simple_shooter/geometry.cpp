#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "geometry.h"

void disegna_cerchio(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / nvertices;
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (i = 0; i < nvertices; i++)
	{
		glBegin(GL_POLYGON);
		glColor4d(colore.r, colore.g, colore.b, colore.a);
		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
		glVertex2f(x + cos((double)(i + 1) *stepA)*raggiox, y + sin((double)(i + 1) *stepA)*raggioy);
		glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
		glVertex2f(x, y);

		glEnd();
	}
}

