﻿// simple_shooter.cpp : definisce il punto di ingresso dell'applicazione console.

#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"
#include "geometry.h"



//float raggiox = 50;
//float raggioy = 50;
//float x = width / 2;
//float y = height / 2;
//
//
//double static lerp(double a, double b, double amount) {
//	return (1 - amount)*a + amount * b;
//}
//
//void disegna_cerchio(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
//	int i;
//	float stepA = (2 * PI) / nvertices;
//	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//
//	for (i = 0; i < nvertices; i++)
//	{
//		glBegin(GL_POLYGON);
//		glColor4d(colore.r, colore.g, colore.b, colore.a);
//		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
//		glVertex2f(x + cos((double)(i + 1) *stepA)*raggiox, y + sin((double)(i + 1) *stepA)*raggioy);
//		glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
//		glVertex2f(x, y);
//
//		glEnd();
//	}
//}
//void disegna_palla(float x, float y, float raggiox, float raggioy) {
//	ColorRGBA colore;
//	ColorRGBA sfumatura;
//	double shadow_scale = lerp(1, 0, raggiox / 200);
//
//	//ombra
//	//c_top = { 0, 0, 0, lerp(64,192,shadow_scale) };
//	//c_bot = { 0, 0, 0, 0 };
//	//draw_ellipse(x - bwidth / 2 * shadow_scale, y - 10 + 25 * (1 - shadow_scale), bwidth*shadow_scale, 50 * shadow_scale, c_top, c_bot);
//
//	//palla
//	colore = { 1, 0, 0, 1 };
//	sfumatura = { 1, 1, 1, 255 };
//	disegna_cerchio(x, y, raggiox, raggioy, colore, sfumatura);
//
//	//ombra
//
//	colore = { 0, 0, 0, 0.2 };
//	sfumatura = { 0.0,0.0,0.0,0.0 };
//	disegna_cerchio(x, y - 50 * shadow_scale, raggiox, raggiox*shadow_scale, colore, sfumatura);
//
//	//speculare
//	/*c_top = { 255, 255, 255, 192 };
//	c_bot = { 255, 255, 255, 0 };
//	draw_ellipse(x - bwidth / 3, y - bheight - height, bwidth / 1.5, bheight / 2, c_top, c_bot);*/
//}



void  main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Prima Applicazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);


	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
}

//int width = 800; // dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonchè del mondo di gioco
//int height = 800;
//#define PI 3.14159265358979323846
//#define nvertices 24
//
//typedef struct { double r, g, b, a; } ColorRGBA; // in formato (255,255,255,255)