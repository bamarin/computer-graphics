#pragma once

// dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonch� del mondo di gioco
#define WIDTH 800
#define HEIGHT 800

/// Drawing routine.
void drawScene(void);

/// OpenGL window reshape routine.
void resize(int w, int h);

/// Callback function per l'evento tastiera
void keyInput(unsigned char key, int x, int y);
