﻿#include "stdafx.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cmath>
#include "routine.h"


// Drawing routine.
void drawScene(void) {
	//Blank color
	glClearColor(0.5, 1.0, 1.0, 0.0);

	//Clean the view with the blank color
	glClear(GL_COLOR_BUFFER_BIT);

	//Draw the grass
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);
		
		glColor3f(0.0, 1.0, 0.0);
		glVertex2f(0.0, 0.0);
		
		glColor3f(0.0, 1.0, 0.1);
		glVertex2f(WIDTH, 0.0);
		
		glColor3f(0.5, 1.0, 0.1);
		glVertex2f(WIDTH, HEIGHT*0.7);
		glVertex2f(0, HEIGHT*0.7);
	glEnd();



	//disegna_palla(x, y, raggiox, raggioy);

	//Fa partire il processo di rendering
	glFlush();
}

// OpenGL window reshape routine.
void resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, WIDTH, 0.0, HEIGHT, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Callback function per l'evento tastiera
void keyInput(unsigned char key, int x, int y){
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}
