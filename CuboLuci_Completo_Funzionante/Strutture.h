#pragma once
#include "Lib.h"
typedef struct {
	float x, y, z;
} Point;
typedef struct {
	float r, g, b, a;
} ColorRGBA;
typedef struct {
 GLuint i,j,k;
} Indici;
typedef struct {
	glm::vec4 Lightposition;
	glm::vec3 AmbientProduct; //a.k.a. the color of the light
	glm::vec3 DiffuseProduct;
	glm::vec3 SpecularProduct;

} Light;
