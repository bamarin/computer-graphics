﻿// CuboLuci.cpp : definisce il punto di ingresso dell'applicazione console.
//


#include "stdafx.h"
#include "Lib.h"
#include "ShaderMaker.h"
#include "strutture.h"
#include "objloader.hpp"


//Impostazione luci
vec4 light1_position(2.0, 0.0, 0.0,1.0);
vec3 light1_ambient(1.0f, 1.0f, 1.0f);
vec3 light1_diffuse(0.7f, 0.7f, 0.7f);
vec3 light1_specular(1.0f,0.0f, 0.0f);
vec4 light2_position(0.0, 3.0, 0.0, 1.0);
vec3 light2_ambient(1.0f, 0.0f, 1.0f);
vec3 light2_diffuse(1.0f, 0.0f, 0.7f);
vec3 light2_specular(1.0f, 0.0f, 0.0f);

Light luci[2];
Light luce1,luce2;

//Informazioni sui materiali
vec3 material_ambient(0.1, 0.2, 0.2);
vec3 material_diffuse(0.4, 0.4, 1.0);
vec3 material_specular(1.0, 0.0, 0.0);
float material_shininess = 1000.0;

//Rotazione intorno agli assi con i tasti x,y,z

float angolox = 0, angoloy = 0, angoloz = 0;

//definizione delle interazioni tra componenti della luce e corrispondente componente del materiale

vec3 ambient_product1 = light1_ambient * material_ambient;
vec3 diffuse_product1 = light1_diffuse * material_diffuse;
vec3 specular_product1 = light1_specular * material_specular;

vec3 ambient_product2= light2_ambient * material_ambient;
vec3 diffuse_product2 = light2_diffuse * material_diffuse;
vec3 specular_product2 = light2_specular * material_specular;

//var glob Modello
vector <vec3> verticesM;
vector <vec2> uvs;
vector <vec3> normalsM;

void costruisci_luci(void)
{

	luce1.Lightposition = light1_position;
	luce1.AmbientProduct = ambient_product1;
	luce1.DiffuseProduct = diffuse_product1;
	luce1.SpecularProduct = specular_product1;

	luce2.Lightposition = light2_position;
	luce2.AmbientProduct = ambient_product2;
	luce2.DiffuseProduct = diffuse_product2;
	luce2.SpecularProduct = specular_product2;

	luci[0]=luce1;
	luci[1]=luce2;


}

//Locazioni variabili uniformi

GLuint MatrixModel, MatrixProj,la[2], ld[2], ls[2], lsh, lp[2],lO;
GLuint MatrixID, MatrixView;

int frame;

GLuint programId;

mat4 Projection;
vec3 camera_pos(0, 0, 5), direzione(0.0, 0.0, -1.0), camera_up(0.0, 1.0, 0.0);

//Definizione dei VAO
GLuint VAO_MODELLO;

//Locazione della matrice ModelViewProjection nello shader


//PArametro telecamera
float angolo_rotY = -90.0f, angolo_rotX = 0.0f, lastX = 400.0, lastY = 400.0;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

vector<Light> luce;

vec3 computeNormal(
	vec3 const & a,
	vec3 const & b,
	vec3 const & c)
{
	return glm::normalize(glm::cross(b - a, c - b));
}




// Initialization routine.
void INIT_VAO(void)
{
	GLenum ErrorCheckValue = glGetError();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	char* vertexShader = (char*)"vertexShader.glsl";
	char* fragmentShader = (char*)"fragmentShader.glsl";

	//Creo il programma in cui sono linkati i due shader compilati
	programId = ShaderMaker::createProgram(vertexShader, fragmentShader);

	//Lo utilizzo

	glUseProgram(programId);

	//Creao il VAO del cubo
	//VAO_CUBO = crea_VAO_Puntatori(VAO_CUBO, verticesC, 8, vertex_colorsC, index_verticesC, 12, normaliC);
	GLuint vertexbuffer, colorbuffer, indices_buffer, normalbuffer;

	glGenVertexArrays(1, &VAO_MODELLO);
	glBindVertexArray(VAO_MODELLO);

	//Cominciamo a Costruire i VBO di vertici, colori e indici dei vertici

	//Genero , rendo attivo, riempio ed assegno al layer 0 la geometria della mia primitva
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, verticesM.size()*sizeof(vec3),&verticesM[0], GL_STATIC_DRAW);

	//Adesso carico il VBO dei vertici nel layer 0
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 

  //Genero , rendo attivo, riempio ed assegno al layer 2 le normali per vertice

	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normalsM.size()*sizeof(vec3), &normalsM[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); //Disattiva il buffer precedentemente attivato 


}


void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	float currentFrame = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (currentFrame - lastFrame) / 1000;
	lastFrame = currentFrame;



	mat4 View;
	//Genero la matrice della Telecamera, (matrice di Vista)
	View = lookAt(camera_pos, camera_pos + direzione, camera_up);


	mat4 Model;

	mat4 MVP;

	glBindVertexArray(VAO_MODELLO);
	Model = mat4(1.0);
	Model = rotate(Model, radians(20.0f), vec3(1.0, 0.0, 0.0));
	Model = rotate(Model, radians(60.0f), vec3(0.0, 1.0, 0.0));
	Model = rotate(Model, radians(angolox), vec3(1.0, 0.0, 0.0));

	Model = rotate(Model, radians(angoloy), vec3(0.0, 1.0, 0.0));

		Model = rotate(Model, radians(angoloz), vec3(0.0, 0.0, 1.0));
		//La postmoltiplico per la matrice dei Scalatura
		Model = scale(Model, vec3(1.5, 1.5, 1.5));

		//Definisco la matrice ModelViewProjection
		mat4 MV = View * Model;
		
		//Assegno il valore attuale delle matrici
		glUniformMatrix4fv(MatrixModel, 1, GL_FALSE, value_ptr(Model));
		glUniformMatrix4fv(MatrixView, 1, GL_FALSE, value_ptr(View));
		glUniformMatrix4fv(MatrixProj, 1, GL_FALSE, value_ptr(Projection));

		//Assegno il valore alla posizione della telecamera.

		glUniform3f(lO, camera_pos.x, camera_pos.y, camera_pos.z);

		//Rendo attivo il VAO del cubo
		glDrawArrays(GL_TRIANGLES, 0, verticesM.size());

	//Rilascio il VABO
	glBindVertexArray(0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glutSwapBuffers();


}
// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);

	//Definisco la matrice di Proiezione
	Projection = perspective(float(radians(45.0)), float(w) / float(h), 0.1f, 100.0f);

}

void MousePassiveMotion(int x, int y)
{
	float xoffset, yoffset;

	//Memorizzo in xoffset la distanza tra l' ascissa della posizione attuale del mouse e quella precedente
	xoffset = x - lastX;
	//Memorizzo in yoffset la distanza tra l' ordinata della posizione attuale del mouse e quella precedente
	yoffset = y - lastY;

	float parametro_sensibilita_mouse = 0.005f;

	//Riduco la distanza in pixel in percentuale

	xoffset *= parametro_sensibilita_mouse;
	yoffset *= parametro_sensibilita_mouse;

	//Memorizzo  la posizione attuale del mouse
	lastX = x;
	lastY = y;

	//Incremento l'angolo di Yaw (ruotazione intorno all'asse y) di una quantit� pari all'incremento subito dalla posizione x del mouse 
	angolo_rotY += xoffset;
	//Incremento l'angolo di Pitch (ruotazione intorno all'asse x) di una quantit� pari all'incremento subito dalla posizione y del mouse 
	angolo_rotX += yoffset;

	//Ricavo la direzone corrispondente all'angolo di Yaw e Pitch aggiornati
	//(facendo uso delle coordinate sferiche)

	vec3 dir_temp;
	dir_temp.x = cos(radians(angolo_rotY))*cos(radians(angolo_rotX));
	dir_temp.y = sin(radians(angolo_rotX));
	dir_temp.z = sin(radians(angolo_rotY))*cos(radians(angolo_rotX));

	//Normalizzo la direzione
	direzione = normalize(dir_temp);
	glutPostRedisplay();

}
// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
/*	float cameraSpeed = deltaTime;

	//Sposto in avanti la camera nella direzione 
	if (key == 'w')
		camera_pos += cameraSpeed*direzione;

	//Sposto indietro la camera nella direzione 
	if (key == 's')
		camera_pos -= cameraSpeed*direzione;

	//Sposto a sinsitra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'a')
		camera_pos -= cameraSpeed*normalize(cross(direzione, camera_up));

	//Sposto a destra la camera (nella direzione ortogonale alla direzione di vista e l'alto della telecamera
	if (key == 'd')
		camera_pos += cameraSpeed*normalize(cross(direzione, camera_up));
		*/
	if (key == 'y')
		angoloy++;


	if (key == 'x')
		angolox++;


	if (key == 'z')
		angoloz++;

	glutPostRedisplay();
}



// Main routine.
int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Camera Libera");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	//evento Mouse senza alcun tasto premuto

	//glutPassiveMotionFunc(MousePassiveMotion);

	glEnable(GL_DEPTH_TEST);

	glewExperimental = GL_TRUE;
	glewInit();
	costruisci_luci();
	bool modello = loadOBJ("lampada.obj", verticesM, uvs, normalsM);
	
	INIT_VAO();

	//Richiesta indirizzi delle variabili Uniformi

	MatrixModel = glGetUniformLocation(programId, "Model");
	MatrixView = glGetUniformLocation(programId, "View");
	MatrixProj = glGetUniformLocation(programId, "Projection");
	

	lp[0] = glGetUniformLocation(programId, "luce[0].Lightposition");
	la[0] = glGetUniformLocation(programId, "luce[0].AmbientProduct");
	ld[0] = glGetUniformLocation(programId, "luce[0].DiffuseProduct");
	ls[0] = glGetUniformLocation(programId, "luce[0].SpecularProduct");

	lp[1] = glGetUniformLocation(programId, "luce[1].Lightposition");
	la[1] = glGetUniformLocation(programId, "luce[1].AmbientProduct");
	ld[1] = glGetUniformLocation(programId, "luce[1].DiffuseProduct");
	ls[1] = glGetUniformLocation(programId, "luce[1].SpecularProduct");


	
	lsh = glGetUniformLocation(programId, "Shininess");
	lO = glGetUniformLocation(programId, "eye_position");


	//Assegnazione dei valori dei parametri unformi

	glUniform3f(la[0], luci[0].AmbientProduct.x, luci[0].AmbientProduct.y, luci[0].AmbientProduct.z);
	glUniform3f(ld[0], luci[0].DiffuseProduct.x, luci[0].DiffuseProduct.x, luci[0].DiffuseProduct.z);
	glUniform3f(ls[0], luci[0].SpecularProduct.x, luci[0].SpecularProduct.y, luci[0].SpecularProduct.z);
	glUniform4f(lp[0], luci[0].Lightposition.x, luci[0].Lightposition.y, luci[0].Lightposition.z,luci[0].Lightposition.w);
	glUniform1f(lsh, material_shininess);

	glUniform3f(la[1], luci[1].AmbientProduct.x, luci[1].AmbientProduct.y, luci[1].AmbientProduct.z);
	glUniform3f(ld[1], luci[1].DiffuseProduct.x, luci[1].DiffuseProduct.x, luci[1].DiffuseProduct.z);
	glUniform3f(ls[1], luci[1].SpecularProduct.x, luci[1].SpecularProduct.y, luci[1].SpecularProduct.z);
	glUniform4f(lp[1], luci[1].Lightposition.x, luci[1].Lightposition.y, luci[1].Lightposition.z, luci[1].Lightposition.w);


	glutMainLoop();
}




