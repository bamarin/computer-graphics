// shadertype=glsl
#version 420 core


layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal;

// Output data ; will be interpolated for each fragment.

out vec3 fragmentColor;

struct Light {
   vec4 Lightposition;
   vec3 AmbientProduct;
   vec3 DiffuseProduct;
   vec3 SpecularProduct;
};

// Values that stay constant for the whole mesh.
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform Light luce[2];

uniform float Shininess;
uniform float eye_position;

void main(){	

	
	//Trasforma le coordinate 8sistema di riferimento locale) del vertice   nelle coordinate del mondo

	vec3 posM = (Model* vec4(vertexPosition_modelspace,1)).xyz;
	//Direzione di vista
	vec3 V=normalize(eye_position-posM);

	//Metto la normale al vertice nelle coordinate del mondo e la normalizzo
    vec3 N = normalize( Model*vec4(vertexNormal,0.0) ).xyz;

	int i=0;

	fragmentColor=vec3(0.0);

	for(int i=0;i<2;++i)
	{
	
	//Normalizzo la direzione della luce
	vec3 L = normalize(luce[i].Lightposition.xyz - posM );
   	
	//Calcolo la direzione di riflessione
	   	vec3 R = -normalize(reflect(L,N) );
	
	

  	//Componente ambientale
    vec3 ambient = luce[i].AmbientProduct;


	//Componente diffusiva
    float coeffD = max( dot(L, N), 0.0 );
    vec3  diffuse = coeffD*luce[i].DiffuseProduct;

	//Componente speculare
    float coeffS = pow( max(dot(R, V), 0.0), Shininess );
    vec3  specular = coeffS * luce[i].SpecularProduct;
    
   
   //Accumulo i contributi delle componenti di ogni luce che influenzano il colore del vertice
	fragmentColor+=(ambient+diffuse+specular);
	}

	//Calcola le coordinate del vertice nelle coordinate di clipping

	gl_Position =  Projection*View*Model*vec4(vertexPosition_modelspace,1);

	
}
