﻿// GiocoVertexArray.cpp : definisce il punto di ingresso dell'applicazione console.
//
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
# include <GL/glew.h>
# include <GL/freeglut.h>


#define PI 3.14159265358979323846
#define nvertices 24
using namespace std;

int width = 1280; // dimensioni iniziali della finestra 
int height = 720;
int frame = 0;
float dxnemici = 0;
float dynemici = 0;
float NumeroColpiti = 0;

double VelocitaOrizzontale = 0; //velocita orizzontale (pixel per frame)
int scuoti = 0;
double accelerazione = 1; // forza di accelerazione data dalla tastiera
double decelerazione = 1; //decelerazione in assenza di input
float posx = width / 2; //coordinate sul piano della posizione iniziale della navicella
float  posy = height * 0.1;
float posizione_di_equilibrio = posy;
float angolo = 0;
typedef struct { double r, g, b, a; } ColorRGBA; // in formato (255,255,255,255)
string Ncolpiti;
float pos_proiettile_MONDO_x, pos_proiettile_MONDO_y; 											 // da radianti in gradi
float posx_Proiettile, posy_Proiettile;

int nemici_per_riga = 10;
int numero_di_righe = 8;
bool luna_colpita = false;

bool **colpito;
float xluna = 320;
float yluna = 640;


typedef struct {
	float x, y, z;
} Point;

Point *v_montagna;
ColorRGBA *ColoriM, *colore_luna, *colore_alone, *coloreC, *coloreCN;
Point *luna, *alone_luna;
Point* ufo, *nemici;
// da gradi in radianti

double  degtorad(double angle) {
	return angle * PI / 180;
}

double  radtodeg(double angle) {
	return angle / (PI / 180);
}


float angoloUfo = 0;

double range_fluttuazione = 15; // fluttuazione su-gi 
double angle = 0; // angolo di fluttuazione
double angle_offset = 10; // quanto   accentuata l'oscillazione angolare

double float_yoffset = 0; // distacco dalla posizione d'equilibrio 
float posxN, posyN;

int frame_animazione = 0; // usato per animare la fluttuazione

int n_vertici = 0;
// valore assoluto
double static Abs(double num) {
	return num < 0 ? -num : num;
}

// ritorna un numero casuale pescato in un certo range
double  random_range(double min, double max) {
	return min + static_cast <double> (rand()) / (static_cast <double> (RAND_MAX / (max - min)));
}


string floatToString(float value) {
	stringstream ss;
	ss << value;
	return ss.str();
}

void bitmap_output(int x, int y, int z, string s,
	void *font)
{
	int i;
	glRasterPos3f(x, y, 0);

	for (i = 0; i < s.length(); i++)
		glutBitmapCharacter(font, s[i]);
}


ColorRGBA color_metaldark = { 110.0 / 255.0 ,110.0 / 255.0 ,110.0 / 255.0,255.0 / 255.0 };
ColorRGBA color_metallight = { 184.0 / 255.0,184.0 / 255.0,184.0 / 255.0,255.0 / 255.0 };

ColorRGBA color_cabindark = { 66.0 / 255.0,134.0 / 255.0,244.0 / 255.0,255.0 / 255.0 };
ColorRGBA color_cabinlight = { 158.0 / 255.0,195.0 / 255.0,255.0 / 255.0,255.0 / 255.0 };


void costruisci_cerchio(float cx, float cy, float raggiox, float raggioy, Point *Cerchio) {
	int i;
	float stepA = (2 * PI) / nvertices;


	int componenti = 0;
	for (i = 0; i < nvertices; i++)
	{
		Cerchio[componenti].x = cx + cos((double)i *stepA)*raggiox;

		Cerchio[componenti].y = cy + sin((double)i *stepA)*raggioy;
		Cerchio[componenti].z = 0;
		Cerchio[componenti + 1].x = cx + cos((double)(i + 1) *stepA)*raggiox;
		Cerchio[componenti + 1].y = cy + sin((double)(i + 1) *stepA)*raggioy;
		Cerchio[componenti + 1].z = 0;
		Cerchio[componenti + 2].x = 0;
		Cerchio[componenti + 2].y = 0;
		Cerchio[componenti + 2].z = 0;

		componenti += 3;

	}

}


//
// TASTI: A D

void updateProiettile(int value)
{
	//Ascissa del proiettile durante lo sparo
	posx_Proiettile = 0;
	//Ordinata del proettile durante lo sparo
	posy_Proiettile++;

	//L'animazione deve avvenire finch  l'ordinata del proiettile raggiunge un certo valore fissato
	if (posy_Proiettile <= 50)
		glutTimerFunc(5, updateProiettile, 0);
	else
		posy_Proiettile = 0;

	glutPostRedisplay();


}

void updateNemici(int value)
{
	//Ascissa del proiettile durante lo sparo
	frame++;

	if (frame % 30 == 0)
	{
		dxnemici--;
		dynemici--;


	}

	glutTimerFunc(5, updateNemici, 0);


	glutPostRedisplay();


}
bool pressing_left = false;
bool pressing_right = false;
bool pressing_attack = false;
bool pressing_rotate_s = false;
bool pressing_rotate_d = false;

void keyboardPressedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case ' ':
		pressing_attack = true;
		updateProiettile(0);
		break;

	case 'a':
		pressing_left = true;
		break;

	case 'd':
		pressing_right = true;
		break;

	case 'r':
		pressing_rotate_s = true;
		break;

	case 'f':
		pressing_rotate_d = true;
		break;
	case 27:
		exit(0);
		break;

	default:
		break;
	}
}

void keyboardReleasedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case ' ':
		pressing_attack = false;
		break;
	case 'a':
		pressing_left = false;
		break;

	case 'd':
		pressing_right = false;
		break;


	case 'r':
		pressing_rotate_s = false;
		break;

	case 'f':
		pressing_rotate_d = false;
		break;
	default:
		break;
	}
}


double lerp(double a, double b, double amount) {

	//Interpolazione lineare tra a e b secondo amount

	return (1 - amount)*a + amount * b;
}



void update(int a)
{
	bool moving = false;
	//Movimento della palla in orizzontale

	if (pressing_left)
	{
		VelocitaOrizzontale -= accelerazione;
		moving = true;
	}

	if (pressing_right)
	{
		VelocitaOrizzontale += accelerazione;
		moving = true;
	}

	if (float_yoffset >= 0) {
		frame_animazione += 6;
		if (frame_animazione >= 360) {
			frame_animazione -= 360;
		}
	}

	if (!moving) {   //Se non mi sto muovendo con i tasti a e d decremento od incremento la velocita' iniziale fino a portarla
					 // a zero e la navicella continua ad oscillare sul posto

		if (VelocitaOrizzontale > 0)
		{
			VelocitaOrizzontale -= 1;
			if (VelocitaOrizzontale < 0)
				VelocitaOrizzontale = 0;
		}

		if (VelocitaOrizzontale < 0)
		{
			VelocitaOrizzontale += 1;
			if (VelocitaOrizzontale > 0)
				VelocitaOrizzontale = 0;
		}
	}

	if (pressing_rotate_s)
	{
		angoloUfo++;
		moving = true;
	}


	if (pressing_rotate_d)
	{
		angoloUfo--;
		moving = true;
	}


	//Aggioramento della posizione in x della navicella, che regola il movimento orizzontale

	posx += VelocitaOrizzontale;



	//Se la navicella assume una posizione in x minore di 0 o maggiore di width dello schermo
	//facciamo rimbalzare la navicella ai bordi dello schermo

	if (posx < 0) {
		posx = 0;
		VelocitaOrizzontale = -VelocitaOrizzontale * 0.8;
	}

	if (posx > width) {
		posx = width;
		VelocitaOrizzontale = -VelocitaOrizzontale * 0.8;
	}

	// calcolo y come somma dei seguenti contributi: pos. di equilibrio, oscillazione periodica
	posy = posizione_di_equilibrio + sin(degtorad(frame_animazione))*range_fluttuazione;
	angolo = cos(degtorad(frame_animazione))*angle_offset - VelocitaOrizzontale * 1.3;


	glutPostRedisplay();
	glutTimerFunc(15, update, 0);
}





void costruisci_ufo(Point* ufo, ColorRGBA *coloreC, bool nemico) {

	//Costruzione della geometria dell'ufo: tutti i vertici in una sola struttura
	Point *corpo1, *corpo2;
	corpo1 = new Point[72];
	corpo2 = new Point[72];

	costruisci_cerchio(0, 0, 1, 1, ufo);

	for (int i = 0; i < nvertices; i++)
	{
		coloreC[i * 3] = color_metaldark;
		coloreC[i * 3 + 1] = color_metaldark;
		coloreC[i * 3 + 2] = color_metallight;
	}

	costruisci_cerchio(0.05, 0.1, 0.7, 0.7, corpo1);

	int cont = 72;
	for (int i = 0; i < 72; i++)
	{
		ufo[cont + i] = corpo1[i];
	}

	for (int i = 0; i < nvertices; i++)
	{
		coloreC[cont + i * 3] = { 1,1,1,32. / 255 };
		coloreC[cont + i * 3 + 1] = { 1,1,1,32. / 255 };
		coloreC[cont + i * 3 + 2] = { 1,1,1,32. / 255 };
	}

	//Costruisci decorazione
	cont = 144;
	ufo[cont].x = 0;
	ufo[cont].y = 0;
	ufo[cont].z = 0;
	ufo[cont + 1].x = 1;
	ufo[cont + 1].y = 1;
	ufo[cont + 1].z = 0;
	ufo[cont + 2].x = -1;
	ufo[cont + 2].y = 1;
	ufo[cont + 2].z = 0;

	//Colore della decorazione

	if (nemico)
	{
		coloreC[cont] = { 0,0,1,0.4 };
		coloreC[cont + 1] = { 0,0.4,1,1 };
		coloreC[cont + 2] = { 0,0.2,1,1 };
	}
	else
	{
		coloreC[cont] = { 1,0,1,0.4 };
		coloreC[cont + 1] = { 1,0.4,1,1 };
		coloreC[cont + 2] = { 1,0.2,1,1 };
	}
	//Costruisci cabina
	cont = 147;

	costruisci_cerchio(0.0, 0.2, 0.5, 0.6, corpo2);
	for (int i = 0; i < 72; i++)
	{
		ufo[cont + i] = corpo2[i];
	}

	for (int i = 0; i < nvertices; i++)
	{
		coloreC[cont + i * 3] = color_cabindark;
		coloreC[cont + i * 3 + 1] = color_cabindark;
		coloreC[cont + i * 3 + 2] = color_cabinlight;

	}

	cont = 219;
	//Proiettile
	ufo[cont].x = 0.0;
	ufo[cont].y = 0.0;
	ufo[cont].z = 0.0;
	coloreC[cont] = { 1.0,1.0,1.0,1.0 };
}

// Drawing routine.

void costruisci_scena(void)
{

	//Costruisce la geometria di tutta la scena: viene chiamato nel main, fuori dal ciclo di MainLoop
	//che forza il ridisegno 	
	int VerticiUfo = 220;

	coloreC = new ColorRGBA[VerticiUfo];

	coloreCN = new ColorRGBA[VerticiUfo];

	luna = new Point[72];
	colore_luna = new ColorRGBA[72];


	alone_luna = new Point[72];
	colore_alone = new ColorRGBA[72];

	ColorRGBA c_top;
	c_top = { 255,255,255,255 };
	ColorRGBA c_bot;
	c_bot = { 255,220,255,255 };

	costruisci_cerchio(0, 0, 1, 1, luna);
	for (int i = 0; i < nvertices; i++)
	{
		colore_luna[i * 3] = c_top;
		colore_luna[i * 3 + 1] = c_top;
		colore_luna[i * 3 + 2] = c_bot;

	}

	c_top = { 255,255,255,0 };
	c_bot = { 255,220,255,255 };
	costruisci_cerchio(0, 0, 1, 1, alone_luna);

	for (int i = 0; i < nvertices; i++)
	{
		colore_alone[i * 3] = c_top;
		colore_alone[i * 3 + 1] = c_top;
		colore_alone[i * 3 + 2] = c_bot;

	}
	ufo = new Point[VerticiUfo];
	costruisci_ufo(ufo, coloreC, true);

	nemici = new Point[VerticiUfo];
	costruisci_ufo(nemici, coloreCN, false);
}


void drawScene(void)
{
	ColorRGBA c_top;
	ColorRGBA c_bot;
	//Definisce il colore con cui verr�  ripulito lo schermo
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//Pulisce lo schermo con il colore definito con la funzione glClearColor
	glClear(GL_COLOR_BUFFER_BIT);

	glPointSize(10);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	//Disegna Luna
	if (scuoti > 0) {

		glPushMatrix();
		glTranslated(random_range(-scuoti, scuoti), random_range(-scuoti, scuoti), 0);
		glPushMatrix();
		glTranslatef(xluna, yluna, 0);
		glScalef(30, 30, 1);
		//Costruisce un array di vertici: primo argomento indica le coordinate di ogni vertice della montagna, la seconda componente
		// il tipo, (FLOAT, INT; DOUBLE), il terzo indica il sizeof di ogni vertice (tipo Point), il terzo il puntatore 
		// alla struttura a partire dalla quale si costruita il vertex array.*/

		glVertexPointer(3, GL_FLOAT, sizeof(Point), luna);
		glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), colore_luna);
		glDrawArrays(GL_TRIANGLES, 0, 72);
		glPopMatrix();

		//Disegna Alone luce
		glPushMatrix();
		glTranslatef(320, 640, 0);
		glScalef(80, 80, 1);
		glVertexPointer(3, GL_FLOAT, sizeof(Point), alone_luna);
		glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), colore_alone);
		glDrawArrays(GL_TRIANGLES, 0, 72);
		glPopMatrix();
		glPopMatrix();
		scuoti = 0;
	}

	{

		glPushMatrix();
		glTranslatef(xluna, yluna, 0);
		glScalef(30, 30, 1);
		//Costruisce un array di vertici: primo argomento indica le coordinate di ogni vertice della montagna, la seconda componente
		// il tipo, (FLOAT, INT; DOUBLE), il terzo indica il sizeof di ogni vertice (tipo Point), il terzo il puntatore 
		// alla struttura a partire dalla quale si costruita il vertex array.*/

		glVertexPointer(3, GL_FLOAT, sizeof(Point), luna);
		glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), colore_luna);
		glDrawArrays(GL_TRIANGLES, 0, 72);
		glPopMatrix();

		//Disegna Alone luce
		glPushMatrix();
		glTranslatef(320, 640, 0);
		glScalef(80, 80, 1);
		glVertexPointer(3, GL_FLOAT, sizeof(Point), alone_luna);
		glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), colore_alone);
		glDrawArrays(GL_TRIANGLES, 0, 72);
		glPopMatrix();
	}

	//Disegna Ufo



	//Disegna Nemici
	float passo_Nemici = ((float)width) / nemici_per_riga;
	float passo_righe = 50;
	for (int i = 0; i < numero_di_righe; i++)
	{
		posyN = height - i * passo_righe - 20;
		for (int j = 0; j < nemici_per_riga; j++)
		{
			posxN = j * (passo_Nemici)+passo_Nemici / 2;
			if (!colpito[i][j]) {
				glPushMatrix();
				glTranslatef(posxN, posyN, 0);
				glTranslatef(dxnemici, dynemici, 0);
				glRotatef(angolo, 0, 0, 1);
				glScalef(55, 22.5, 1);
				glVertexPointer(3, GL_FLOAT, sizeof(Point), nemici);
				glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), coloreCN);

				glDrawArrays(GL_TRIANGLES, 0, 219);

				glPopMatrix();


			}
		}
	}

	for (int i = 0; i < numero_di_righe; i++)
	{
		posyN = height - i*passo_righe - 20 + dynemici;
		for (int j = 0; j < nemici_per_riga; j++)
		{
			posxN = j*(passo_Nemici)+passo_Nemici / 2 + dxnemici;
			if (((pos_proiettile_MONDO_x >= posxN - 20) && (pos_proiettile_MONDO_x <= posxN + 20)) && ((pos_proiettile_MONDO_y >= posyN - 20) && (pos_proiettile_MONDO_y <= posyN + 20)))
			{
				if (!colpito[i][j])
				{
					NumeroColpiti++;
					colpito[i][j] = true;
				}


			}

		}
	}
	glPushMatrix();
	glTranslatef(posx, posy, 0);
	glScalef(55, 22.5, 1);
	glRotatef(180, 0, 0, 1);
	glRotatef(angolo, 0, 0, 1);

	glVertexPointer(3, GL_FLOAT, sizeof(Point), ufo);
	glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), coloreC);
	glDrawArrays(GL_TRIANGLES, 0, 219);

	glPopMatrix();

	glPointSize(10.0);
	glPushMatrix();
	glTranslatef(posx, posy, 0);
	glScalef(55, 22.5, 1);
	glRotatef(180, 0, 0, 1);
	glTranslatef(posx_Proiettile, -posy_Proiettile, 0.0);
	glVertexPointer(3, GL_FLOAT, sizeof(Point), ufo);
	glColorPointer(4, GL_DOUBLE, sizeof(ColorRGBA), coloreC);
	glDrawArrays(GL_POINTS, 219, 1);

	float matrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	//coordinate del proiettile nel sistema di riferimento del modello
	float mp[4];
	mp[0] = 0;
	mp[1] = 0.0;
	mp[2] = 0;
	mp[3] = 1;

	float pos_p[4];
	for (int i = 0; i < 4; i++)
		pos_p[i] = 0;


	for (int i = 0; i<4; i++)
		for (int j = 0; j<4; j++)
			pos_p[i] += matrix[j * 4 + i] * mp[j];


	//COORDINATE DEL PROIETTILE NEL SISTEMA DI RIFERIMENTO DEL MONDO
	pos_proiettile_MONDO_x = pos_p[0];
	pos_proiettile_MONDO_y = pos_p[1];
	glPopMatrix();


	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);



	if (((pos_proiettile_MONDO_x >= xluna - 30) && (pos_proiettile_MONDO_x <= xluna + 30)) && ((pos_proiettile_MONDO_y >= yluna - 30) && (pos_proiettile_MONDO_y <= yluna + 30)))
		scuoti = 10;
	//Aggiorno sulla finestra il numero di nuvole colpite
	glColor3f(1.0, 0.0, 0.0);
	string testo = "Nemici Colpiti ";
	//Trasformo in stringa il numero di nuvole colpite
	Ncolpiti = floatToString(NumeroColpiti);
	//Appendo alla stringa di testo che viene visualizzata il numero aggiornato di nuvole colpite
	testo.append(Ncolpiti.substr(0, 4));
	bitmap_output(350, 20, 0, testo, GLUT_BITMAP_TIMES_ROMAN_24);



	glutSwapBuffers();



}


// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void setup() {

}

int main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(3, 0);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Animazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	colpito = new bool*[numero_di_righe];
	for (int i = 0; i < numero_di_righe; i++)
		colpito[i] = new bool[nemici_per_riga];

	for (int i = 0; i < numero_di_righe; i++)
		for (int j = 0; j < nemici_per_riga; j++)
		{
			colpito[i][j] = false;
			printf("%s", colpito[i][j] ? "true \n" : "false \n");
		}

	//Evento tastiera tasi premuti
	glutKeyboardFunc(keyboardPressedEvent);
	//Evento tastiera tasto rilasciato

	glutKeyboardUpFunc(keyboardReleasedEvent);

	costruisci_scena();

	//gestione animazione
	glutTimerFunc(11, update, 0);
	glutTimerFunc(24, updateNemici, 0);


	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
	
	return 1;
}



