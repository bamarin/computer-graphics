#pragma once
#include "stdafx.h"

#ifndef GEOMETRY
#define GEOMETRY

/// Group of 4 numbers in range (0-255) as components of an RGBA color
typedef struct { double r, g, b, a; } ColorRGBA; //(255,255,255,255)

void drawCircle(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura);

void disegna_montagne(float x, float y, int altezza_montagna, int larghezza_montagne, int numero_di_montagne, ColorRGBA color_top, ColorRGBA color_bot);

#endif //GEOMETRY