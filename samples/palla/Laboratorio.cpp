﻿// Laboratorio.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include <stdio.h>
# include <GL/glew.h>
# include <GL/freeglut.h>
#include <cmath>
#define PI 3.14159265358979323846
#define nvertices 24

int width = 1280; // dimensioni del canvas, che corrispondono alle dimensioni iniziali della finestra nonchè del mondo di gioco
int height = 720;

// parametri della palla
int ball_height = 0; // distacco da terra (negativo)
double ball_gravity = 1.5; // forza di gravità
double ball_bounciness = -20; //impulso con cui si stacca da terra: forza del rimbalzo
double ball_squishyness = 20; //quanti pixel la palla si schiaccia col terreno
double ball_viscosity = abs(ball_bounciness) / 4.8; // moltiplicatore che rallenta l'animazione del rimbalzo
double ball_vspeed = ball_bounciness; //velocità verticale (pixel per frame)
double ball_hspeed = 0; //velocità orizzontale (pixel per frame)
double ball_maxspeed = 22; // velocità di movimento orizzontale massima

double ball_acceleration = 1; // forza di accelerazione data dalla tastiera
double ball_linear_damping = 1; //decelerazione in assenza di input
float ball_x = width / 2; //coordinate sul piano (l'asse y è invertito rispetto alle coordinate di OpenGL)
float  ball_y = height*0.2;
typedef struct { double r, g, b, a; } ColorRGBA; // in formato (255,255,255,255)


												 // TASTI: S D

bool pressing_left = false;
bool pressing_right = false;

void keyboardPressedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{


	case 'a':
		pressing_left = true;
		break;


	case 'd':
		pressing_right = true;
		break;

	case 27:
		exit(0);
		break;

	default:
		break;
	}
}

void keyboardReleasedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{


	case 'a':
		pressing_left = false;
		break;


	case 'd':
		pressing_right = false;
		break;

	default:
		break;
	}
}


double lerp(double a, double b, double amount) {
	return (1 - amount)*a + amount * b;
}


void disegna_cerchio(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / nvertices;
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (i = 0; i < nvertices; i++)
	{
		glBegin(GL_POLYGON);
		glColor4d(colore.r, colore.g, colore.b, colore.a);
		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
		glVertex2f(x + cos((double)(i + 1) *stepA)*raggiox, y + sin((double)(i + 1) *stepA)*raggioy);
		glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
		glVertex2f(x, y);

		glEnd();
	}
}
void disegna_palla(float x, float y, int ball_height) {
	ColorRGBA colore;
	ColorRGBA sfumatura;
	double shadow_scale = lerp(1, 0, (float)ball_height / 255);

	double bwidth = ball_height < 0 ? lerp(80, 100, (double)abs(ball_height) / ball_squishyness) : 80; // larghezza effettiva in pixel della palla
	double bheight = ball_height < 0 ? 80 + ball_height : 80; // altezza effettiva in pixel della palla

	if (shadow_scale < 0)
		shadow_scale = 0;


	colore = { 1, 0, 0, 1 };
	sfumatura = { 1, 0.78, 0, 1 };
	disegna_cerchio(x - bwidth / 2, y + bheight + ball_height, bwidth / 2, bheight / 2, colore, sfumatura);


	//ombra
	sfumatura = { 0, 0, 0, lerp(64,255,shadow_scale) };
	colore = { 0.5, 0.5, 0.5, 0 };
	disegna_cerchio(x - bwidth / 2 * shadow_scale, y + 10 + 10 * (1 - shadow_scale), (bwidth*shadow_scale), (50 * shadow_scale), colore, sfumatura);
}

void disegna_piano(float x, float y, float width, float height, ColorRGBA color_top, ColorRGBA color_bot) {

	glBegin(GL_POLYGON);

	glColor4d(color_top.r / 255, color_top.g / 255, color_top.b / 255, color_top.a / 255);
	glVertex2f(x, y);
	glVertex2f(x + width, y);

	glColor4d(color_bot.r / 255, color_bot.g / 255, color_bot.b / 255, color_bot.a / 255);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);

	glEnd();
}
// Drawing routine.
void drawScene(void)
{
	ColorRGBA c_top;
	ColorRGBA c_bot;
	//Definisce il colore con cui verr� ripulito lo schermo
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//Pulisce lo schermo con il colore definito con la funzione glClearColor
	glClear(GL_COLOR_BUFFER_BIT);


	//Disegna cielo
	c_top = { 77,165,255,255 };
	c_bot = { 0,127,255,255 };

	disegna_piano(0, height / 2, width, height / 2, c_top, c_bot);


	//Disegna prato
	disegna_piano(0, 0, width, height / 2, { 34,139,34,255 }, { 173,255,47,255 });


	



	//Fa partire il processo di rendering
	glFlush();
}

// Initialization routine.
void setup(void)
{
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}




// Callback function per l'evento tastiera
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}



int main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(3, 0);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Animazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);



	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
		return 0;
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
	return 1;
}
