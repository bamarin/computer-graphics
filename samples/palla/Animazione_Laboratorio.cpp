﻿// Animazione.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
# include <GL/glew.h>
# include <GL/freeglut.h>
#include <cmath>
#define PI 3.14159265358979323846
#define nvertices 24

int width = 1280; // dimensioni iniziali della finestra 
int height = 720;

// parametri della palla
int distacco_da_terra = 0; // distacco da terra (negativo)
double forzaGravita = 1.5; // forza di gravita
double 	VelocitaVerticale = 20; //velocita verticale (pixel per frame)
double VelocitaOrizzontale = 0; //velocita orizzontale (pixel per frame)
double velocitaMassima = 22; // velocita di movimento orizzontale massima

double accelerazione = 1; // forza di accelerazione data dalla tastiera
double decelerazione = 1; //decelerazione in assenza di input
float posx = width / 2; //coordinate sul piano della posizione iniziale della palla
float  posy = height*0.2;
typedef struct { double r, g, b, a; } ColorRGBA; // in formato (255,255,255,255)



												 // TASTI: A D

bool pressing_left = false;
bool pressing_right = false;

void keyboardPressedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'a':
		pressing_left = true;
		break;

	case 'd':
		pressing_right = true;
		break;

	case 27:
		exit(0);
		break;

	default:
		break;
	}
}

void keyboardReleasedEvent(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'a':
		pressing_left = false;
		break;

	case 'd':
		pressing_right = false;
		break;

	default:
		break;
	}
}


double lerp(double a, double b, double amount) {

	//Interpolazione lineare tra a e b secondo amount

	return (1 - amount)*a + amount * b;
}

void update(int a)
{
	bool moving = false;
	//Movimento della palla in orizzontale

	if (pressing_left)
	{
		VelocitaOrizzontale -= accelerazione;
		moving = true;
	}

	if (pressing_right)
	{
		VelocitaOrizzontale += accelerazione;
		moving = true;
	}


	
	if (!moving) {   //Se non mi sto muovendo con i tasti a e d decremento od incremento la velocita' iniziale fino a portarla
		// a zero e la palla continua a rimbalzare sul posto

		if (VelocitaOrizzontale > 0)
		{
			VelocitaOrizzontale -= 1;
			if (VelocitaOrizzontale < 0)
				VelocitaOrizzontale = 0;
		}

		if (VelocitaOrizzontale < 0)
		{
			VelocitaOrizzontale+= 1;
			if (VelocitaOrizzontale > 0)
				VelocitaOrizzontale = 0;
		}
	}

//Aggioramento della posizione in x della pallina, che regola il movimento orizzontale

	posx += VelocitaOrizzontale;



	//Se la pallina assume una posizione in x minore di 0 o maggiore di width dello schermo
	//facciamo rimbalzare la pallina ai bordi dello schermo

	if (posx < 0) {
		posx = 0;
		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
	}

	if (posx >width) {
		posx = width;
		VelocitaOrizzontale = -VelocitaOrizzontale*0.8;
	}

	// Gestione del rimalzo e quindi dell'altezza da terra

	//Rimbalzo
	VelocitaVerticale += forzaGravita;

	distacco_da_terra += VelocitaVerticale;

	if (distacco_da_terra > 20)
	{
		distacco_da_terra = 20;
		VelocitaVerticale = -20;   //Una volta giunta a terra la pallina ottiene un impulso negativo che la ritornare su
	}

	glutPostRedisplay();
	glutTimerFunc(24, update, 0);
}

void disegna_montagne(float x, float y, int altezza_montagna, int larghezza_montagne, int numero_di_montagne, ColorRGBA color_top, ColorRGBA color_bot)
{

	int i = 0;
	int Numero_di_pezzi = 128;

	float dimensione_pezzi = larghezza_montagne /(float) Numero_di_pezzi;
	float frequenza = PI*numero_di_montagne;

	for (i = 0; i < Numero_di_pezzi; i++)
	{
		glBegin(GL_POLYGON);
		glColor4d(color_top.r / 255, color_top.g / 255, color_top.b / 255, color_top.a / 255);

		glVertex2f(x + i*dimensione_pezzi, y + altezza_montagna*abs(sin(i / (float)Numero_di_pezzi*frequenza)));

		glVertex2f(x + (i+1)*dimensione_pezzi, y + altezza_montagna*abs(sin((i+1) / (float)Numero_di_pezzi*frequenza)));

		glColor4d(color_bot.r / 255, color_bot.g / 255, color_bot.b / 255, color_bot.a / 255);

		glVertex2f(x + (i + 1)*dimensione_pezzi, y);

		glVertex2f(x + i*dimensione_pezzi, y);

		glEnd();
	}
}
void disegna_cerchio(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	float stepA = (2 * PI) / nvertices;

	for (i = 0; i < nvertices; i++)
	{
		glBegin(GL_POLYGON);
		glColor4d(colore.r/255, colore.g/255, colore.b/255, colore.a/255);
		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
		glVertex2f(x + cos((double)(i + 1) *stepA)*raggiox, y + sin((double)(i + 1) *stepA)*raggioy);
		glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
		glVertex2f(x, y);

		glEnd();
	}
}
void disegna_palla(float x, float y, int ball_height) {
	ColorRGBA colore;
	ColorRGBA sfumatura;
	double shadow_scale = lerp(1, 0, (float)ball_height / 255);

	double bwidth = ball_height < 0 ? lerp(80, 100, (double)abs(ball_height) /20) : 80; // larghezza effettiva in pixel della palla
	double bheight = ball_height < 0 ? 80 + ball_height : 80; // altezza effettiva in pixel della palla

	if (shadow_scale < 0)
		shadow_scale = 0;


	colore = { 255, 0, 0, 255 };
	sfumatura = {255, 204, 0, 255 };
	disegna_cerchio(x - bwidth / 2, y + bheight + ball_height, bwidth / 2, bheight / 2, colore, sfumatura);


	//ombra
	sfumatura = { 0, 0, 0, lerp(64,255,shadow_scale) };
	colore = { 125, 125, 125, 0 };
	disegna_cerchio(x - bwidth / 2 * shadow_scale, y + 10 + 10 * (1 - shadow_scale), (bwidth*shadow_scale), (50 * shadow_scale), colore, sfumatura);
}

void disegna_piano(float x, float y, float width, float height, ColorRGBA color_top, ColorRGBA color_bot) {

	glBegin(GL_POLYGON);

	glColor4d(color_top.r / 255, color_top.g / 255, color_top.b / 255, color_top.a / 255);
	glVertex2f(x, y);
	glVertex2f(x + width, y);

	glColor4d(color_bot.r / 255, color_bot.g / 255, color_bot.b / 255, color_bot.a / 255);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);

	glEnd();
}


// Drawing routine.
void drawScene(void)
{
	ColorRGBA c_top;
	ColorRGBA c_bot;
	//Definisce il colore con cui verrà  ripulito lo schermo
	glClearColor(0.0, 0.0, 0.0, 0.0);

	//Pulisce lo schermo con il colore definito con la funzione glClearColor
	glClear(GL_COLOR_BUFFER_BIT);


	//Disegna cielo
	c_top = { 77,165,255,255 };
	c_bot = { 0,127,255,255 };

	disegna_piano(0, height / 2, width, height / 2, c_top, c_bot);

	//Disegna Sole
	c_top = { 255,255,255,255 };
	c_bot = { 255,220,0,255 };
	disegna_cerchio(320, 640, 30, 30, c_top, c_bot);

	//Disegna Luce
	c_top = { 255,255,255,0 };
	c_bot = { 255,220,0,255 };
	disegna_cerchio(320, 640, 80, 80, c_top, c_bot);

	//Disegna prato
	disegna_piano(0, 0, width, height / 2, { 34,139,34,255 }, { 173,255,47,255 });

	//Disegna Montagne

	disegna_montagne(0, height / 2, 100, width, 5, {139,69,19,255 }, { 255,127,9,255 });

	
	//Disegna palla

	disegna_palla(posx, posy, -distacco_da_terra);
	//Fa partire il processo di rendering
	glFlush();
}

// Initialization routine.
void setup(void)
{
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}




int main(int argc, char **argv)
{

	GLboolean GlewInitResult;

	glutInit(&argc, argv);

	glutInitContextVersion(3, 0);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Animazione");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);

	//Evento tastiera tasi premuti
	glutKeyboardFunc(keyboardPressedEvent);
	//Evento tastiera tasto rilasciato

	glutKeyboardUpFunc(keyboardReleasedEvent);


	//gestione animazione
	glutTimerFunc(66, update, 0);


	GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult) {
		fprintf(
			stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult)
		);
		exit(EXIT_FAILURE);
		return 0;
	}

	fprintf(
		stdout,
		"INFO: OpenGL Version: %s\n",
		glGetString(GL_VERSION)
	);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutMainLoop();
	return 0;
}

