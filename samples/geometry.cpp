# include <GL/glew.h>
# include <GL/freeglut.h>
#include <cmath>
#include "geometry.h"
#include "stdafx.h"

#define CIRLCE_VERTICES 24
#define PI 3.14159265358979323846

typedef struct { double r, g, b, a; } ColorRGBA; //(255,255,255,255)

void drawCircle(float x, float y, float raggiox, float raggioy, ColorRGBA colore, ColorRGBA sfumatura) {
	int i;
	double stepA = (2 * PI) / CIRLCE_VERTICES;
	glBegin(GL_TRIANGLE_FAN);
	/*center of the circle*/
	glColor4f(sfumatura.r, sfumatura.g, sfumatura.b, sfumatura.a);
	glVertex2f(x, y);	
	/* circle vertices */
	for (i = 0; i <= CIRLCE_VERTICES; i++)
	{
		glColor4d(colore.r, colore.g, colore.b, colore.a);
		glVertex2f(x + cos((double)i *stepA)*raggiox, y + sin((double)i *stepA)*raggioy);
	}
	glEnd();
}

void disegna_montagne(float x, float y, int altezza_montagna, int larghezza_montagne, int numero_di_montagne, ColorRGBA color_top, ColorRGBA color_bot)
{
	int i = 0;
	int Numero_di_pezzi = 8;

	float dimensione_pezzi = larghezza_montagne / (float)Numero_di_pezzi;
	float frequenza = PI*numero_di_montagne;

	for (i = 0; i < Numero_di_pezzi; i++)
	{
		glBegin(GL_POLYGON);
		glColor4d(color_top.r / 255, color_top.g / 255, color_top.b / 255, color_top.a / 255);

		glVertex2f(x + i*dimensione_pezzi, y + altezza_montagna*abs(sin(i / (float)Numero_di_pezzi*frequenza)));

		glVertex2f(x + (i + 1)*dimensione_pezzi, y + altezza_montagna*abs(sin((i + 1) / (float)Numero_di_pezzi*frequenza)));

		glColor4d(color_bot.r / 255, color_bot.g / 255, color_bot.b / 255, color_bot.a / 255);

		glVertex2f(x + (i + 1)*dimensione_pezzi, y);

		glVertex2f(x + i*dimensione_pezzi, y);

		glEnd();
	}
}